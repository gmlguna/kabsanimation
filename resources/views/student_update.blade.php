

@extends('layout.main')
@section('title', 'Autocad | Animation Center in Krishnagiri')
@section('keywords', 'Animation Center in Krishnagiri')
@section('description', 'Animation Center in Krishnagiri')
@section('content')


    <h2 class="student_subhead" style="background-color:lightslategrey;"> <div class="welcome"><strong class="welcomekbas"> Welcome to Kabs animation </strong>
            <div class="right_logout">
                <a class="logout" href="{{ url('/staff_registerdetails') }}"><strong>StaffDetails</strong></a>
                <a class="logout" href="{{ url('/studentregister_details') }}"><strong>StudentDetails</strong></a>
                <a class="logout" href="{{ url('/traningdetails') }}"><strong>TraningClass Details</strong></a>
                <a class="logout" href="{{ url('/empenquire') }}"><strong>EnquireDetails</strong></a>
                <a class="logout" href ="employe"><strong>Logout</strong></a>
            </div>
        </div>
    </h2>

    <h4>
        <center style="color:red;">
            @if(session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif
        </center>
    </h4>



        <form action="/student_edit/<?php echo $users[0]->regno; ?>" method="post">
            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

            <table  border ="8">

                <tr class="boldtable" bgcolor="#d3d3d3">
                    <th>Register No:</th>

                    <td>
                        <input type='text' name='regno'
                               value='<?php echo $users[0]->regno; ?>'/>
                    </td>
                </tr>
                <tr class="boldtable" bgcolor="#d3d3d3">
                    <th>Name</th>

                    <td>
                        <input type='text' name='name'
                               value='<?php echo $users[0]->name; ?>'/>
                    </td>
                </tr>


                <tr class="boldtable" bgcolor="#d3d3d3">
                    <th>DOB</th>

                    <td>
                        <input type='text' name='dob'
                               value='<?php echo $users[0]->dob; ?>'/>
                    </td>
                </tr>

                <tr class="boldtable" bgcolor="#d3d3d3">
                    <th>MobileNo</th>

                    <td>
                        <input type='text' name='mobileno'
                               value='<?php echo $users[0]->mobileno; ?>'/>
                    </td>
                </tr>

                <tr class="boldtable" bgcolor="#d3d3d3">
                    <th>Email</th>

                    <td>
                        <input type='text' name='email'
                               value='<?php echo $users[0]->email; ?>'/>
                    </td>
                </tr>

                <tr class="boldtable" bgcolor="#d3d3d3">
                    <th>Gender</th>
                    <td>
                        <input type='text' name='gender'
                               value='<?php echo $users[0]->gender; ?>'/>
                    </td>
                </tr>

                <tr class="boldtable" bgcolor="#d3d3d3">
                    <th>Startdate</th>
                    <td>
                        <input type='text' name='startdate'
                               value='<?php echo $users[0]->startdate; ?>'/>
                    </td>
                </tr>
                <tr class="boldtable" bgcolor="#d3d3d3">

                    <th>Enddate</th>
                    <td>
                        <input type='text' name='enddate'
                               value='<?php echo $users[0]->enddate; ?>'/>
                    </td>

                </tr>

                <tr class="boldtable" bgcolor="#d3d3d3">
                    <th>Duration</th>
                    <td>
                        <input type='text' name='duration'
                               value='<?php echo $users[0]->duration; ?>'/>
                    </td>
                </tr>

                <tr class="boldtable" bgcolor="#d3d3d3">
                    <th>Joindate</th>
                    <td>
                        <input type='text' name='joindate'
                               value='<?php echo $users[0]->joindate; ?>'/>
                    </td>
                </tr>
                <tr class="boldtable" bgcolor="#d3d3d3">

                    <th>Courses</th>
                    <td>
                        <input type='text' name='courses'
                               value='<?php echo $users[0]->courses; ?>'/>
                    </td>
                </tr>
                <tr class="boldtable" bgcolor="#d3d3d3">
                    <th>Address</th>

                    <td>
                        <input type='text' name='address'
                               value='<?php echo $users[0]->address; ?>'/>

                    </td>
                </tr>

                <tr bgcolor="#d3d3d3">
                    <th></th>

                    <td>
                        <input type='submit' value="Update "/>
                    </td>
                </tr>




            </table>
        </form>



@endsection





