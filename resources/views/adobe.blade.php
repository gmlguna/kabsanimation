@extends('layout.main')
@section('title', 'Adobe | Animation Center in Krishnagiri')
@section('keywords', 'Animation Center in Krishnagiri')
@section('description', 'Animation Center in Krishnagiri')
@section('content')
    <div class="container-fluid">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 academy_class">
                <h2 class="adobe_courses"><strong>Adobe Courses</strong></h2>
                <div class="course_information_paragraph">Scroll over the icons to find out more
                </div>
                <input id="woocommerce-product-search-field-0" class="search-field" placeholder="Search products…" value="" name="s" type="search">
                <div class="course_information_paragraph">If you want to work in the creative industries, you’ll want to work with the Adobe. Master the tools of
                    the trade and you’ll be well on the way to becoming a computer games or graphic designer, motion picture
                    editor or marcomms executive. kabs animation Class, an Authorized Adobe center, will help you get your career
                    to the next level. Centres in London, Glasgow, Leeds & Manchester.

                    ADOBE TRAINING LONDON

                </div>

                <div class="container-fluid ss_sanpshot">
                    <div class="container">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 training_courses">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null" href="{{ url('/') }}">
                                    <button type="button" class="phonenumberses"> <div class="letter_adobe">ADOBE TRAINING LONDON</div> </button>
                                </a>
                            </div>
                            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 training_courses">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null" href="{{ url('/') }}">
                                    <button type="button" class="phonenumberses"> <div class="letter_adobe">ADOBE TRAINING MANCHESTER</div> </button>
                                </a>
                            </div>
                            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 training_courses">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null" href="{{ url('/') }}">
                                    <button type="button" class="phonenumberses"> <div class="letter_adobe">ADOBE TRAINING GLASGOW</div> </button>
                                </a>
                            </div>
                            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 training_courses">
                                <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null" href="{{ url('/') }}">
                                    <button type="button" class="phonenumberses"> <div class="letter_adobe">ADOBE TRAINING LEEDS</div> </button>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="two"> <strong>Choose your Adobe courses below.</strong></div>
            </div>
        </div>
    </div>
@endsection