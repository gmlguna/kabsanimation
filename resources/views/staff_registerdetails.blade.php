@extends('layout.main')
@section('title', 'Autocad | Animation Center in Krishnagiri')
@section('keywords', 'Animation Center in Krishnagiri')
@section('description', 'Animation Center in Krishnagiri')
@section('content')
    <h2 class="student_subhead" style="background-color:lightslategrey;"> <div class="welcome"><strong class="welcomekbas"> Welcome to Kabs animation </strong>
            <div class="right_logout">
                <a class="logout" href="{{ url('/staff_registerdetails') }}"><strong>StaffDetails</strong></a>
                <a class="logout" href="{{ url('/studentregister_details') }}"><strong>StudentDetails</strong></a>
                <a class="logout" href="{{ url('/traningdetails') }}"><strong>TraningClass Details</strong></a>
                <a class="logout" href="{{ url('/empenquire') }}"><strong>EnquireDetails</strong></a>
                <a class="logout" href ="employe"><strong>Logout</strong></a>
            </div>
        </div>
    </h2>


    <div class="container-fluid">
        <div class="container table_size">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h6>Staff Details</h6>
                    <div class="container table_size">
                        <table bgcolor="#bdb76b" border ="8" class="table">
                            <thead>
                            <tr class="boldtable" bgcolor="#d3d3d3">
                                <td>Name</td>
                                <td>Dob</td>
                                <td>Mobile NO</td>
                                <td>Email</td>
                                <td>Gender</td>
                                <td>Joindate</td>
                                <td>Address </td>
                                <td>Image_upload</td>
                            </tr>
                            </thead>
                            @foreach ($users as $user)
                                <tr>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->dob }}</td>
                                    <td>{{ $user->mobileno }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->gender }}</td>
                                    <td>{{ $user->joindate }}</td>
                                    <td>{{ $user->address }}</td>
                                    <td> <img src="storage/{{$user->image_upload}}" alt="" width="50" height="50"/> </td>
                                    {{--<td><a href = 'seleced/{{ $user->id }}'button class="button button5">Purchase</a></td>--}}

                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <h1>
        <table>
            <tr>
                <a href ="/staff_register"><button class="buttons buttons2">New Register</button></a>
                <a href ="/views/staff_delete"><button class="buttons buttons2">Delete</button></a>
                <a href ="/views/staff"><button class="buttons buttons2">update</button></a>
                {{--<a href ="/seleced"><button class="button button2">Select</button></a>--}}
                </td>
            </tr>
        </table>
    </h1>

@endsection