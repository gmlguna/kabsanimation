@extends('layout.main')
@section('title', 'Autocad | Animation Center in Krishnagiri')
@section('keywords', 'Animation Center in Krishnagiri')
@section('description', 'Animation Center in Krishnagiri')
@section('content')

        <h2 class="student_subhead" style="background-color:lightslategrey;">
            <div class="welcome"><strong class="welcomekbas"> Welcome to Kabs Animation </strong>
                <div class="right_logout">
                    <a class="logoutcer" href="{{ url('/check/'.$regno) }}"> DownloadCertification </a>
                    <a class="logout" href="userlogin"><strong>Logout</strong></a>
                </div>
            </div>
        </h2>

    <div class="container-fluid profile certification_border">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                    <h6 class="login_display">Name</h6>
                    <h6 class="login_display">Course</h6>
                    <h6 class="login_display">Register #</h6>
                    <h6 class="login_display">Admission Date</h6>
                    <h6 class="login_display">Date of Birth</h6>
                    <h6 class="login_display">Email</h6>
                    <h6 class="login_display">Start Date</h6>
                    <h6 class="login_display">End Date</h6>
                    <h6 class="login_display">Duration</h6>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                    <h6 class="login_displays"><strong>{{$login->name}}</strong></h6>
                    <h6 class="login_displays"><strong>{{$login->courses}}</strong></h6>
                    <h6 class="login_displays"><strong>{{$login->regno}}</strong></h6>
                    <h6 class="login_displays"><strong>{{$login->joindate}}</strong></h6>
                    <h6 class="login_displays"><strong>{{$login->dob}}</strong></h6>
                    <h6 class="login_displays"><strong>{{$login->email}}</strong></h6>
                    <h6 class="login_displays"><strong>{{$login->startdate}}</strong></h6>
                    <h6 class="login_displays"><strong>{{$login->enddate}}</strong></h6>
                    <h6 class="login_displays"><strong>{{$login->duration}}</strong></h6>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                    <h6 class="login_display"><img src="storage/{{$login->image_upload}}" alt="" width="100"
                                                   height="100"/></h6><br>
                    <h6 class="login_display">Gender</h6>
                    <h6 class="login_display">Mobile No #</h6>
                    <h6 class="login_display">Address</h6>
                </div>

                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 whats_link">
                    <h6 class="login_displays"><strong></strong></h6>
                    <h6 class="login_displays"><strong></strong></h6>
                    <h6 class="login_displays"><strong></strong></h6>
                    <h6 class="login_displays"><strong></strong></h6>
                    <h6 class="login_displays"><strong></strong></h6> <br>
                    <br>
                    <h6 class="login_displays"><strong>{{$login->gender}}</strong></h6>
                    <h6 class="login_displays"><strong>{{$login->mobileno}}</strong></h6>
                    <h6 class="login_displays"><strong>{{$login->address}}</strong></h6>
                </div>
            </div>
        </div>
    </div>
    <div class="admin_buttons">
    </div>


@endsection



&nbsp;







