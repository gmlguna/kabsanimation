@extends('layout.main')
@section('title', 'Captivate | Animation Center in Krishnagiri')
@section('keywords', 'Animation Center in Krishnagiri')
@section('description', 'Animation Center in Krishnagiri')
@section('content')

    <div class="container-fluid after_home common_bg_style">
        <h4>
            <center style="color:red;">
                @if(session()->has('message'))
                    <div class="alert alert-success">
                        {{ session()->get('message') }}
                    </div>
                @endif
            </center>
        </h4>
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 adobe_anim">
                    <strong> Adobe Captivate <br>Training Courses</strong>
                    <div class="animate_form"> Adobe Captivate is the ideal authoring tool for creating interactive and
                        multimedia-filled eLearning content, quizzes, video tutorials and software simulations that can be
                        published as video, responsive webpage content (viewable on all devices) or as a stand-alone App.
                        18-month Free class retake included.
                    </div>
                    <h5 class="adobe_animass"><strong> Pick a Captivate course or package below.
                            Unsure which Captivate course level will be best for you?<a href="{{ url('/') }}"> <font color="#bfd432">
                                    Click here </font> </a>
                            to take our free online skills assessment and find out!</strong>
                    </h5>
                    <img src="{{URL::asset('image/after/adobe.jpg') }}" width="150" height="50">
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="after_videoss">
                        <iframe class="iframeses" src="https://www.youtube.com/embed/Xp-SWDMkTMk?feature=oembed"></iframe>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 choose_your">
                    <div class="descript"><strong>Class Snapshots:</strong></div>
                    <img class="adobe" src="{{URL::asset('image/sanpshot/1.jpg') }}"><br>
                    <img class="adobe" src="{{URL::asset('image/sanpshot/2.jpg') }}"><br>
                    <img class="adobe" src="{{URL::asset('image/sanpshot/3.jpg') }}"><br>
                    <img class="adobe" src="{{URL::asset('image/sanpshot/4.jpg') }}"><br>
                    <img class="adobe" src="{{URL::asset('image/sanpshot/5.jpg') }}"><br>
                    <img class="adobe" src="{{URL::asset('image/sanpshot/6.jpg') }}"><br>
                    <img class="adobe" src="{{URL::asset('image/sanpshot/7.jpg') }}"><br>
                    <img class="adobe" src="{{URL::asset('image/sanpshot/3.jpg') }}"><br>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 course_information">
                    <h2><strong>Information on courses:</strong></h2>
                    <div class="description"><strong>Full description</strong></div>
                    <div class="course_information_paragraph">At kabs animation  Class, we have provided numerous professionals with
                        Adobe Captivate training to help build file’s, create video tutorials, application simulations,
                        branching scenarios, software demos and tests with multimedia and interactivity, etc. and take
                        advantage of Adobe Captivate’s advanced features. Our expert Adobe certified Adobe Captivate
                        trainers have years of experience to impart quality knowledge on the software to students.
                    </div>
                    <div class="course_information_paragraph">We run custom Adobe Captivate courses tailored to meet your
                        precise prerequisites. Whether you are a novice at Adobe Captivate or an experienced user, kabs animation
                        Class’ trainers will deliver you a highly professional education. We make sure you learn what you
                        need to know to create high quality e-learning experiences.
                    </div>
                    <div class="course_information_paragraph">kabs animation  Class brings qualified and knowledgeable professional
                        Adobe Captivate trainers, quality course curriculum, latest computers, and small class sizes to give
                        extra attention to your training; and ensure you get your money’s worth from your investment in
                        Adobe approved Adobe Captivate training course.
                    </div>
                    <div class="course_information_paragraph">Our instructors at kabs animation  Class use a task-based approach to
                        teach the important features of Adobe Captivate and apply industry designing’s best practices
                        throughout with in-class projects to design successful Adobe Captivate projects; and ensure that you
                        learn to build continuous e-learning projects that use Adobe Captivate Templates.
                    </div>
                    <div class="course_information_paragraph">Adobe Captivate training courses at kabs animation  Class mingles
                        lectures, demonstrations and hands-on practices, providing plentiful opportunities to fulfil your
                        specific requirements and facilitates you in distributing content nearly anyplace. Our Adobe
                        certified instructors with their expertise help you achieve a meticulous foundation in Adobe
                        Captivate. On completion of the Adobe Captivate training course you will have ample skill and
                        assurance to begin creating your own simulations, demos and e-learning tutorials.
                    </div>
                    <div class="course_information_paragraph">kabs animation  Class provides you high quality Adobe Captivate
                        training course and guarantees that it is a satisfying learning experience with us. We also offer
                        you free of charge re-sit of the course, within 18 months of your first day of training to refresh
                        what you’ve learned.
                    </div>
                    <div class="course_information_paragraph">All our classes at kabs animation  Class begin at 9.30am and running
                        till 4:30pm. Enrol now in one of the best Captivate, Adobe-authorised training courses.
                    </div>

                    <div class="description"><strong>Blended Learning</strong></div>
                    <div class="course_information_paragraph">It’s the best opportunity to get the most out of your learning
                        experience while blending technology with classroom instructions. We supply training videos, notes
                        and/or reference texts.
                    </div>
                    <div class="description"><strong>18-month Free Class Retake</strong></div>
                    <div class="course_information_paragraph">If you have any gaps in your knowledge or want to refresh your
                        skills, you are more than welcome to come back and retake the live online class free of charge up to
                        18 months after you have taken the class.
                    </div>
                    <div class="description"><strong>Money-Back Guarantee</strong></div>
                    <div class="course_information_paragraph">If you don’t absolutely LOVE your class, we’ll give you a full
                        refund! Let us know on the FIRST day of your training if something isn’t quite right and give us a
                        chance to fix it or give you your money back.
                    </div>
                    <div class="description"><strong>Funding</strong></div>
                    <div class="course_information_paragraph">Because we’re committed to your success, we’re offering you
                        the opportunity to pay for your training monthly, rather than the whole cost upfront.
                    </div>

                    <a href="{{ url('/') }}">  <div class="more_info"> <strong> Click here for more information</strong> </div> </a>

                    <div class="description"><strong>Experienced Instructors</strong></div>
                    <div class="course_information_paragraph">Equipped with years of industry experience our instructors
                        will assure a successful leap in your knowledge, improvement and preparation.
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="container-fluid log_form common_bg_styless">
        <form action="/home" method="post">
            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
            <div class="container">
                <div class="enq"><strong>Enquire now!</strong></div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <select id="location" class="input" name="location">
                            <option value="location">Choose Location</option>
                            <option value="krishnagiri">Krishnagiri</option>
                            <option value="vellore">Vellore</option>
                            <option value="tirupattur">Tirupattur</option>
                            <option value="hosur">hosur</option>
                            <option value="chennai">chennai</option>
                        </select>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 list">
                        <fieldset class="{{ $errors->has('company') ? ' has-error' : '' }}">
                            <input type="text" id="company" name="company" class="input" placeholder="enter the company..">
                            @if ($errors->has('company'))<span
                                    class="help-block error_font"><strong>{{ $errors->first('company') }}</strong></span>@endif
                        </fieldset>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 list">
                        <fieldset class="{{ $errors->has('name') ? ' has-error' : '' }}">
                            <input type="text" id="name" name="name" class="input" placeholder="enter the name..">
                            @if ($errors->has('name'))<span
                                    class="help-block error_font"><strong>{{ $errors->first('name') }}</strong></span>@endif
                        </fieldset>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 list">
                        <fieldset class="{{ $errors->has('email') ? ' has-error' : '' }}">
                            <input type="text" id="email" name="email" class="input" placeholder="enter the email..">
                            @if ($errors->has('email'))<span
                                    class="help-block error_font"><strong>{{ $errors->first('email') }}</strong></span>@endif
                        </fieldset>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 list">
                        <fieldset class="{{ $errors->has('phone') ? ' has-error' : '' }}">
                            <input type="text" id="phone" name="phone" class="input" placeholder="enter the phone..">
                            @if ($errors->has('phone'))<span
                                    class="help-block error_font"><strong>{{ $errors->first('phone') }}</strong></span>@endif
                        </fieldset>
                        <br>
                        <br>
                        <input type="text" id="date" name="date" class="input" readonly value="<?php echo date("d / m / y");?>">
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 list">
                        <textarea name="yourmsg"  placeholder="Your message*" class="input" required></textarea>
                        <div class="special_offers">
                            <input type="checkbox" name="checkbox" value="">
                            I would like to get news about courses and special offers</div>
                        <button input type="submit" name="submit" class="btn">ENQUIRE NOW</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="container-fluid share_with">
        <div class="container">
            <div class="share"><strong>Share with:</strong></div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 images_icon">
                    <a href="{{ url('https://twitter.com/') }}"> <img alt="twitter" title="twitter"  src="{{URL::asset('image/twitter.png') }}" width="60" height="40"> </a>
                    <a href="{{ url('http://fb.com/') }}"> <img alt="fb3" title="fb3" src="{{URL::asset('image/fb3.png') }}" width="60" height="40"> </a>
                    <a href="{{ url('https://www.linkedin.com/') }}">  <img alt="in" title="in" src="{{URL::asset('image/in.png') }}" width="60" height="40"> </a>
                </div>
            </div>
        </div>
    </div>

@endsection