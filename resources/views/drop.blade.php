<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{URL::asset('css/bootstrap.css') }}">
</head>
<style>
    li {
        font-size: 17px;
        text-decoration: none;
    }

    .dropdown-menu > li > a {
        padding: 10px 10px;
    }

    .dropdown-menu .sub-menu {
        left: 100%;
        top: 0;
        visibility: hidden;
        margin-top: -1px;
    }

    .sub-menu li a:hover {
        opacity: 0.9;
        background-color: black;
    }

    a:hover, button:hover{
        opacity: 0.9!important;

    }

    .dropdown-menu li a:hover {
        opacity: 0.9;
        color: #66b2ff!important;
        background-color: black;
    }

    .dropdown-menu li:hover .sub-menu {
        visibility: visible;
        background-color: #f9f9f9;
        font-size: 14px;
    }

    .dropdown:hover .dropdown-menu {
        display: inline;
        background-color:white;
        font-size: 14px;
    }

    .dropdown-content {
        display: inline;
        position: absolute;
        background-color: #f9f9f9;
        min-width: 160px;
        box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
        z-index: 1;
    }
    .dropdown-menu {
        display: inline!important;
    }


    .dropdown-menu {
        position: absolute;
        top: 100%;
        z-index: 1000;
        display: none;
        left: 15px;
        min-width: 264px!important;
        font-size: 14px;
        list-style: none;
        background-color: #fff;
        border: 1px solid #ccc;
        border-radius: 3px;
        -webkit-box-shadow: 0 6px 12px rgba(0,0,0,.175);
        box-shadow: 0 6px 12px rgba(0,0,0,.175);
        background-clip: padding-box;
    }

    .dropdown-menu {
        padding: 0!important;
    }


</style>



<body>


<div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 padd_top_20 our_company left">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 billing_partner">
        <ul>
            <li class="active">
                <a href="http://wordpress.lan/services/">Services Overview</a>
            </li>
            <li class="simple">
                <a href="http://wordpress.lan/medical-billing/">Medical Billing</a>
            </li>
            <li class="simple">
                <a href="http://wordpress.lan/mental-health-billing/">Mental Health Billing</a>
            </li>
            <ul class="nav navbar-nav">
                <ul class="dropdown-menu">
                    <li>
                        <a href="{{ url('/adobe') }}">ADOBE <i class="icon-arrow-right"></i></a>
                        <ul class="dropdown-menu  sub-menu">
                            <li><a href="{{ url('/expresiencedesign') }}">Acrobat pro DC</a></li>
                            <li><a href="{{ url('/after_effects') }}">After Effects</a></li>
                            <li><a href="{{ url('/animate') }}">Animate</a></li>
                            <li><a href="{{ url('/captivate') }}">Captivate</a></li>
                            <li><a href="{{ url('/creativecloud') }}">Creative Cloud </a></li>
                        </ul>
                    </li>
                </ul>
            </ul>
        </ul>
    </div>

</div>


</body>
</html>

