@extends('layout.main')
@section('title', 'Certification | Animation Center in Krishnagiri')
@section('keywords', 'Animation Center in Krishnagiri')
@section('description', 'Animation Center in Krishnagiri')
@section('content')
    <div class="container-fluid certificate_bg">
        <div class="container">
            <ul class="nav nav-tabs adobe_animatess">
                <li class="active"><a data-toggle="tab" href="{{ url('/#tab1') }}">ADOBE</a></li>
                <li><a data-toggle="tab" href="{{ url('/#tab2') }}">AUTODESK</a></li>
                <li><a data-toggle="tab" href="{{ url('/#tab3') }}">UNITY</a></li>
                <a class="student_login" href="{{ url('/userlogin') }}"><strong> <button class="buttonss buttonss2">Student Login</button> </strong> </a>
            </ul>


            <div class="tab-content">
                <div id="tab1" class="tab-pane fade in active">
                    <div class="container">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_top_two_em">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                <img src="{{URL::asset('image/adobecer.jpg') }}" width="240" height="280">
                            </div>

                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 course_inform">
                                <div class="certification_size"><strong>Adobe</strong></div>
                                <div class="certification_paragraph">ADOBE’S CERTIFICATION PROGRAMS MEET A WIDE VARIETY OF
                                    NEEDS.WHETHER YOU ARE AN ASPIRING STUDENT, A DIGITAL MEDIA PROFESSIONAL OR AN EDUCATOR, YOU’LL
                                    FIND THAT BEING CERTIFIED MAKES A DIFFERENCE.
                                </div>
                                <div class="certification_paragraph">Give yourself a promotion: become an Adobe Certified
                                    Associate (ACA) or an Adobe Certified Expert (ACE) and set yourself apart from other creatives
                                    and IT professionals. Consistently demonstrate expertise with Adobe products and platforms
                                    through certification. Would you like to stand out and differentiate your knowledge on Adobe
                                    products? Learn more about how you can become an ACA or ACE.
                                </div>
                                <div class="certification_paragraph">As an individual, an Adobe Certified credential allows you
                                    to:
                                </div>
                                <ul>
                                    <li>Differentiate yourself from competitors</li>
                                    <li>Get your CV noticed</li>
                                    <li>Attract and win new business</li>
                                    <li>Find the right person for the job</li>
                                    <li>Quickly assess candidate skill level</li>
                                </ul>

                                <div class="certification_paragraph">As a business, use the Adobe Certified credentials as a
                                    benchmark so you can:
                                </div>
                                <ul>
                                    <li>Find the right person for the job</li>
                                    <li>Quickly assess candidate skill level</li>
                                    <li>Invest in and promote your most promising employees</li>
                                </ul>
                                <div class="certification_paragraph">
                                    <strong> ADOBE CERTIFIED ASSOCIATE CERTIFICATION</strong></div>
                                <a href="{{ url('/photoshop') }}"> <img src="https://www.academyclass.com/wp-content/uploads/2016/04/icons_37.jpg"
                                                  width="30" height="25"></a><font size="3" color="#c5d43e"> Photoshop
                                    <a href="{{ url('/illustrator') }}"> <img src="https://www.academyclass.com/wp-content/uploads/2016/04/icons_80-1.jpg"
                                                      width="30" height="25"></a>  Illustrator
                                    <a href="{{ url('/indesign') }}"> <img src="https://www.academyclass.com/wp-content/uploads/2016/04/icons_56-1.jpg"
                                                      width="30" height="25"></a>  InDesign
                                    <a href="{{ url('/premierepro') }}"> <img src="https://www.academyclass.com/wp-content/uploads/2016/04/icons_51.jpg"
                                                      width="30" height="25"></a>  Premiere Pro</font>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="tab2" class="tab-pane fade">
                    <div class="container">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_top_two_em">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                <img src="{{URL::asset('image/autodeskcer.jpg') }}" width="240" height="280">
                            </div>

                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 course_inform">
                                <div class="certification_size"><strong>Autodesk</strong></div>

                                <div class="certification_paragraph"><strong>Autodesk Certification can help you succeed in your
                                        design
                                        career—providing benefits to both you and your employer.</strong>
                                </div>
                                <div class="certification_paragraph">A reliable validation of your skills and knowledge, Autodesk
                                    Certification can lead to accelerated professional development, improved productivity and
                                    enhanced credibility in your field.
                                </div>
                                <div class="certification_paragraph">In addition to improving your skill set, earning Autodesk
                                    Certification provides the following advantages and benefits:
                                </div>
                                <ul>
                                    <li><strong>Enhanced Skills and Recognition</strong> – Autodesk Certification provides
                                        validation of your skills
                                        and knowledge along with industry recognition of your achievements.
                                    </li>
                                    <li><strong>Electronic Certificate</strong> – Once certified, you receive access to an
                                        electronic certificate
                                        suitable for printing and framing. You may reprint this certificate as often as you like.
                                        Your certificate is available anytime through your Autodesk Online system profile.
                                    </li>
                                    <li><strong>Certification Logo</strong> – After passing the certifica­tion exam, you receive the
                                        official
                                        Autodesk Certification logo for use on business cards, résumés, and letterhead, identifying
                                        you as Autodesk Certified. Your logo kit is available through your Autodesk Online system
                                        profile
                                    </li>
                                    <li><strong>Immediate Diagnostic Feedback</strong> – Whether or not you pass the exam, you
                                        receive immediate
                                        feedback on areas to improve your Autodesk application skills. Your score report with
                                        feedback is available at any time through your Autodesk Online system profile.
                                    </li>
                                    <li><strong>Listing on Autodesk in the Certified Users Database </strong>– Once certified, you
                                        can choose to have
                                        your contact information and certifications listed in Autodesk’s publically available
                                        database. This is an excellent way to immediately demonstrate your certification status to
                                        prospective employers.
                                    </li>
                                </ul>
                                <div class="certification_paragraph">
                                    <strong> CERTIFIED USER EXAMS</strong></div>
                                <a href="{{ url('/dsmax') }}"> <img src="{{URL::asset('image/after/autodesk1.png') }}" width="150" height="30"> </a>
                                <a href="{{ url('/autocad') }}"> <img src="{{URL::asset('image/after/autodesk2.png') }}" width="150" height="30"> </a>
                                <a href="{{ url('/fusion') }}"> <img src="{{URL::asset('image/after/autodesk3.png') }}" width="150" height="30"> </a><br>
                                <br>
                                <br>
                                <br>
                                <a href="{{ url('/inventor') }}"> <img src="{{URL::asset('image/after/autodesk4.png') }}" width="150" height="30"> </a>
                                <a href="{{ url('/maya') }}"> <img src="{{URL::asset('image/after/autodesk5.png') }}" width="150" height="30"> </a>
                                <a href="{{ url('/revit') }}"> <img src="{{URL::asset('image/after/autodesk6.png') }}" width="200" height="30"> </a>
                                <div class="certification_paragraph">
                                    <strong> CERTIFIED USER EXAMS</strong></div>
                                <a href="{{ url('/dsmax') }}"> <img src="{{URL::asset('image/after/autodesk1.png') }}" width="150" height="30"> </a>
                                <a href="{{ url('/autocad') }}"> <img src="{{URL::asset('image/after/autodesk2.png') }}" width="150" height="30"> </a>
                                <a href="{{ url('/inventor') }}"> <img src="{{URL::asset('image/after/autodesk4.png') }}" width="150" height="30"> </a><br>
                                <br>
                                <br>
                                <br>
                                <a href="{{ url('/dsmax') }}"> <img src="{{URL::asset('image/after/autodesk1.png') }}" width="200" height="30"> </a>
                                <a href="{{ url('/fusion') }}"> <img src="{{URL::asset('image/after/autodesk3.png') }}" width="150" height="30"> </a><br>
                                <br>
                                <br>
                                <a href="{{ url('/revit') }}"> <img src="{{URL::asset('image/after/autodesk8.png') }}" width="150" height="30"> </a>
                                <br>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="tab3" class="tab-pane fade">
                    <div class="container">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_top_two_em">
                            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                                <img src="{{URL::asset('image/unitycer.png') }}" width="230" height="280">
                            </div>

                            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12 course_inform">
                                <div class="certification_size"><strong>Unity</strong></div>
                                <div class="certification_paragraph"><strong>Get Unity Certified</strong></div>
                                <div class="certification_paragraph">Stand out from the crowd and improve your chances of
                                    landing that dream job by becoming a Unity Certified Developer.
                                </div>
                                <div class="certification_paragraph">Validate your Unity knowledge and skills and earn a
                                    credential to help build your resume and communicate your skills to employers.
                                </div>
                                <div class="certification_size"><strong>Frequently Asked Questions</strong></div>

                                <div class="certification_paragraph"><strong>What’s covered on the exam?</strong></div>
                                <div class="certification_paragraph">The Unity Certified Developer Exam covers foundational
                                    skills for video game development with Unity. The exam is composed of 100 questions spanning
                                    16 areas of competency and is intended to evaluate an individual’s knowledge for a
                                    comprehensive understanding of the game production process. Question formats include
                                    multiple choice, hot spot, drag-and-drop, and matching. For a detailed outline of the topic
                                    areas covered on the exam, please review the <a href="home">
                                        <div class="more_info"> Unity Certified Developer Exam Objectives (PDF)</div>
                                    </a>
                                </div>

                                <div class="certification_paragraph"><strong>How should I prepare for the exam?</strong></div>
                                <div class="certification_paragraph">Some individuals may determine after reviewing the
                                    Unity Certified Developer Exam Objectives that they are ready for the exam based on their
                                    existing
                                    knowledge and experience. Others may feel that they need to complete additional preparatory
                                    learning. The Unity Certified Developer Courseware is recommended but not required for exam
                                    preparation. Courseware is available for purchase online atcertification.unity.com and Unity
                                    Plus, Pro, and Enterprise Subscription customers are eligible for free limited-time access to
                                    the program.
                                </div>

                                <div class="certification_paragraph"><strong>How long is the exam?</strong></div>
                                <div class="certification_paragraph">The exam is approximately 100 questions, with a time limit of
                                    90 minutes. If you do not complete the exam within the allowed time, you will be graded on the
                                    portion you have completed when time runs out.
                                </div>

                                <div class="certification_paragraph"><strong>What should I bring to the exam?</strong></div>
                                <div class="certification_paragraph">Please bring a government-issued photo ID, proof of identity is
                                    required at check in.
                                </div>

                                <div class="certification_paragraph"><strong>Refund & Cancellation Policy</strong></div>
                                <div class="certification_paragraph">Certification Exam registrations are non-transferrable. Refunds
                                    will be issued for cancellations made more than 24 hours in advance of the event. Cancellations
                                    that are received less than 24 hours before the start of the event and no-shows are not eligible
                                    for refund or rescheduling.
                                </div>

                                <div class="certification_paragraph"><strong>PLEASE NOTE!</strong></div>
                                <div class="photoid_exam"><strong>**You MUST bring a photo ID to take the exam**</strong>
                                </div>
                                <div class="certification_paragraph">You must bring along a government-issued photo ID to take the
                                    exam. Computers will be provided on site.
                                </div>


                                <div class="certification_paragraph"><strong>Terms & Conditions</strong></div>
                                <div class="certification_paragraph">Please be sure to review the full Unity Certification Program
                                    Terms & Conditions. Registration for the event constitutes your acknowledgement of an agreement
                                    to these terms.
                                </div>

                                <div class="certification_paragraph"><strong>Have questions about this Unity Certification
                                        Event?</strong></div>
                                <div class="certification_paragraph">Contact Acakabs animation demy Class on 822 045 6017 or email
                                    Admin@kabsanimation.com
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid get_certified">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                <div class="wpb_wrapper cups">
                    <div class="vc_icon_element vc_icon_element-outer wpb_animate_when_almost_visible wpb_left-to-right vc_icon_element-align-center vc_icon_element-have-style wpb_start_animation">
                        <div class="vc_icon_element-inner vc_icon_element-color-white vc_icon_element-have-style-inner vc_icon_element-size-xl vc_icon_element-style-rounded-outline vc_icon_element-outline vc_icon_element-background-color-white">
                            <span class="vc_icon_element-icon entypo-icon entypo-icon-trophy"></span></div>
                    </div>
                    <div class="wpb_text_column wpb_content_element  wpb_animate_when_almost_visible wpb_right-to-left wpb_start_animation">
                        <div class="wpb_wrapper"><h2 style="text-align: center;"><span style="color: #ffffff;"> <img class="cup" src="{{URL::asset('image/cup.png') }}" width="150" height="120"> <br>Why do you need to get certified?</span>
                            </h2></div>
                    </div>
                    <div class="wpb_text_column wpb_content_element ">
                        <div class="wpb_wrapper cup"><p style="text-align: center; text-transform: none;"><span
                                        style="color: #ffffff;">Adobe and Autodesk offer professional certification for people wanting to develop their expertise in the key products within the creative industry. kabs animation  Class offers excellent preparation classes to assist you in your certification goal.</span>
                            </p></div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="container-fluid  Benefits_yourcoure girl_img ">
        <div class="container  certification_home common_bg_style">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                </div>
                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                    <div class="career">
                        <div class="certification_para "><strong>Benefits to your career</strong></div>
                        <ul>
                            <li>Deploying new techniques and ideas straight away</li>
                            <li>Improve your performance and add value to your company</li>
                            <li>Demonstrate your new expertise in the latest technology</li>
                            <li>Certification is a qualification that is always yours &amp; demonstrates that you’re on top of
                                your game
                            </li>
                            <li>Recognition of your true value, bonuses, advancement</li>
                            <li>Industry recognition of your skill set</li>
                            <li>Peer recognition of your expertise</li>
                            <li>Then you join the kabs animation  – Regular webinars, user group meetings and Meetup’s</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid Benefits_yourcoure">
        <div class="container  certification1_home common_bg_style">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                    <div class="career">
                        <div class="certification_para "><strong>Benefits for your employer</strong></div>
                        <ul>
                            <li>Increased productivity from new techniques</li>
                            <li>Benefit by investing in staff – higher key staff retention</li>
                            <li>Be able to market the company to bid for more complex projects</li>
                            <li>Increase productivity by promoting skills that give you edge
                            </li>
                            <li>Competitive advantage – promote the use of certified staff on projects</li>
                            <li>Motivating and retaining staff by improving their skill sets</li>
                            <li>Use certification as a goal for performance rewards</li>
                            <li>Make better hiring decisions</li>
                            <li>Objectively benchmark employee performance</li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid share_with">
        <div class="container">
            <div class="share"><strong>Share with:</strong></div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 images_icon">
                    <a href="{{ url('https://twitter.com/') }}"> <img alt="twitter" title="twitter"  src="{{URL::asset('image/twitter.png') }}" width="60" height="40"> </a>
                    <a href="{{ url('http://fb.com/') }}"> <img alt="fb3" title="fb3" src="{{URL::asset('image/fb3.png') }}" width="60" height="40"> </a>
                    <a href="{{ url('https://www.linkedin.com/') }}">  <img alt="in" title="in" src="{{URL::asset('image/in.png') }}" width="60" height="40"> </a>
                </div>
            </div>
        </div>
    </div>
@endsection