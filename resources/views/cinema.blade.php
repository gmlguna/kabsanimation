
@extends('layout.main')
@section('title', 'Cinema | Animation Center in Krishnagiri')
@section('keywords', 'Animation Center in Krishnagiri')
@section('description', 'Animation Center in Krishnagiri')
@section('content')

    <div class="container-fluid after_home common_bg_style">
        <h4>
            <center style="color:red;">
                @if(session()->has('message'))
                    <div class="alert alert-success">
                        {{ session()->get('message') }}
                    </div>
                @endif
            </center>
        </h4>
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 adobe_anim">
                    <strong>Cinema 4D Courses</strong>
                    <div class="animate_form">Maxon Cinema 4D is a 3D motion graphics toolkit known for its seamless connectivity
                        to popular 2D motion graphics software and compositing tools like Adobe After Effects. Create
                        high-end 3D images and animations for film, TV and broadcast, architecture, games, multimedia,
                        design and engineering. Expand your 3D horizons and try out new features with expert Ackabs animation ademy Class
                        support. 18-month Free class retake included.
                    </div>
                    <h5 class="adobe_animass"><strong>Pick a Cinema 4D course or package below.
                            Unsure which Cinema 4D course level will be best for you? <a href="{{ url('/') }}"> <font color="#bfd432">
                                    Click
                                    here </font> </a> to take our free online skills assessment and find out!
                        </strong>
                    </h5>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="after_videoss">
                        <iframe class="iframeses" src="https://www.youtube.com/embed/58qrP5vXwJg?feature=oembed"></iframe>

                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                    <div class="descript"><strong>Class Snapshots:</strong></div>
                    <img class="adobe" src="{{URL::asset('image/sanpshot/1.jpg') }}"><br>
                    <img class="adobe" src="{{URL::asset('image/sanpshot/2.jpg') }}"><br>
                    <img class="adobe" src="{{URL::asset('image/sanpshot/3.jpg') }}"><br>
                </div>

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 course_information">
                    <h2><strong>Information on courses:</strong></h2>
                    <div class="description"><strong>Full description</strong></div>
                    <div class="course_information_paragraph">Learn Cinema 4D in the shortest time possible at kabs animation
                        Class. Our Cinema 4D courses are designed to aid you in expanding your 3D horizons and to try out
                        new features that you did not even know Cinema 4D was capable of. These courses are prepared to
                        enhance not only your skillfulness but also your earning potential as a 4D expert.
                    </div>
                    <div class="course_information_paragraph">Start to get to grips with this exciting 3D animation,
                        ray-tracing and modeling software with the aid of kabs animation  Class’ Cinema 4D courses. These courses
                        are taught step-by-step by our expert instructors, who lead students through the important Cinema 4D
                        techniques, tools and features, including the menus, workflows and configuration.


                    </div>
                    <div class="course_information_paragraph">With kabs animation  Class, learning Cinema 4D will prove to be a
                        valuable tool to improve your skills and expand your working knowledge of broadcasting, production
                        and animation. These courses are easy to learn and cost-effective. Now you can learn Cinema 4D from
                        an accredited expert in a number of days. As the instructors equip you with the essentials of Cinema
                        4D and disclose several valuable tips that are sure to heighten your creative abilities.
                    </div>
                    <div class="course_information_paragraph">We at kabs animation  Class have designed our courses to make them
                        ideal for beginners and in way that they help you master Maxon Cinema 4D from basics to advanced
                        levels. The Cinema 4D courses at kabs animation  Class use an easy to follow training format that is both
                        engaging and informative. The instructors use a distinctive training method that bring a high degree
                        of visual clarity to even the most complex application and offer accelerated learning and high
                        retention, with the help of expert tutors.
                    </div>
                    <div class="course_information_paragraph">
                        Our highly qualified industry recognised instructors make the Cinema 4D training courses extremely
                        comprehensive and easy to understand. The Cinema 4D trainers at kabs animation  Class are all experienced
                        Cinema 4D experts who share a passion for teaching and sharing their extensive real world industry
                        experience.


                    </div>
                    <div class="course_information_paragraph">
                        kabs animation  Class provides you high quality Cinema 4D training course and guarantees that it is a
                        satisfying learning experience with us. We also offer you free of charge re-sit of the course,
                        within 18 months of your first day of training to refresh what you’ve learned.


                    </div>
                    <div class="course_information_paragraph">
                        All our classes at kabs animation  Class begin at 9.30am and running till 4:30pm.
                    </div>

                    <div class="description"><strong>Blended Learning</strong></div>
                    <div class="course_information_paragraph">It’s the best opportunity to get the most out of your
                        learning
                        experience while blending technology with classroom instructions. We supply training videos,
                        notes
                        and/or reference texts.
                    </div>
                    <div class="description"><strong>18-month Free Class Retake</strong></div>
                    <div class="course_information_paragraph">If you have any gaps in your knowledge or want to refresh
                        your
                        skills, you are more than welcome to come back and retake the live online class free of charge
                        up to
                        18 months after you have taken the class.
                    </div>
                    <div class="description"><strong>Money-Back Guarantee</strong></div>
                    <div class="course_information_paragraph">If you don’t absolutely LOVE your class, we’ll give you a
                        full
                        refund! Let us know on the FIRST day of your training if something isn’t quite right and give us
                        a
                        chance to fix it or give you your money back.
                    </div>
                    <div class="description"><strong>Funding</strong></div>
                    <div class="course_information_paragraph">Because we’re committed to your success, we’re offering
                        you
                        the opportunity to pay for your training monthly, rather than the whole cost upfront.
                    </div>
                    <a href="{{ url('/') }}">
                        <div class="more_info"><strong> Click here for more information</strong></div>
                    </a>
                    <div class="description"><strong>Experienced Instructors</strong></div>
                    <div class="course_information_paragraph">Equipped with years of industry experience our instructors
                        will assure a successful leap in your knowledge, improvement and preparation.
                    </div>
                </div>
            </div>

        </div>
    </div>

    <div class="container-fluid log_form common_bg_styless">
        <form action="/home" method="post">
            <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
            <div class="container">
                <div class="enq"><strong>Enquire now!</strong></div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <select id="location" class="input" name="location">
                            <option value="location">Choose Location</option>
                            <option value="krishnagiri">Krishnagiri</option>
                            <option value="vellore">Vellore</option>
                            <option value="tirupattur">Tirupattur</option>
                            <option value="hosur">hosur</option>
                            <option value="chennai">chennai</option>
                        </select>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 list">
                        <fieldset class="{{ $errors->has('company') ? ' has-error' : '' }}">
                            <input type="text" id="company" name="company" class="input" placeholder="enter the company..">
                            @if ($errors->has('company'))<span
                                    class="help-block error_font"><strong>{{ $errors->first('company') }}</strong></span>@endif
                        </fieldset>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 list">
                        <fieldset class="{{ $errors->has('name') ? ' has-error' : '' }}">
                            <input type="text" id="name" name="name" class="input" placeholder="enter the name..">
                            @if ($errors->has('name'))<span
                                    class="help-block error_font"><strong>{{ $errors->first('name') }}</strong></span>@endif
                        </fieldset>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 list">
                        <fieldset class="{{ $errors->has('email') ? ' has-error' : '' }}">
                            <input type="text" id="email" name="email" class="input" placeholder="enter the email..">
                            @if ($errors->has('email'))<span
                                    class="help-block error_font"><strong>{{ $errors->first('email') }}</strong></span>@endif
                        </fieldset>

                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 list">
                        <fieldset class="{{ $errors->has('phone') ? ' has-error' : '' }}">
                            <input type="text" id="phone" name="phone" class="input" placeholder="enter the phone..">
                            @if ($errors->has('phone'))<span
                                    class="help-block error_font"><strong>{{ $errors->first('phone') }}</strong></span>@endif
                        </fieldset>
                        <br>
                        <br>
                        <input type="text" id="date" name="date" class="input" readonly value="<?php echo date("d / m / y");?>">
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 list">
                        <textarea name="yourmsg"  placeholder="Your message*" class="input" required></textarea>
                        <div class="special_offers">
                            <input type="checkbox" name="checkbox" value="">
                            I would like to get news about courses and special offers</div>
                        <button input type="submit" name="submit" class="btn">ENQUIRE NOW</button>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="container-fluid share_with">
        <div class="container">
            <div class="share"><strong>Share with:</strong></div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 images_icon">
                    <a href="{{ url('https://twitter.com/') }}"> <img alt="twitter" title="twitter"  src="{{URL::asset('image/twitter.png') }}" width="60" height="40"> </a>
                    <a href="{{ url('http://fb.com/') }}"> <img alt="fb3" title="fb3" src="{{URL::asset('image/fb3.png') }}" width="60" height="40"> </a>
                    <a href="{{ url('https://www.linkedin.com/') }}">  <img alt="in" title="in" src="{{URL::asset('image/in.png') }}" width="60" height="40"> </a>
                </div>
            </div>
        </div>
    </div>
@endsection