<html>
<style>
    .computer {
        font-size: 16px;
        text-align: center;
        padding-top: 60px;
        color: orange;
        text-shadow: 2px 1px orange;
        padding-bottom: 25px;
    }

    .kabs {
        padding-top: 40px;
        font-size: 26px;
        color: darkslategrey;
        text-shadow: 2px 1px orange;
        font-style: inherit;
    }

    p {
        font-size: 14px;
        color: black;
        word-spacing: 2px;
        text-indent: 40px;
        line-height: 1.2;
        font-style: inherit;
    }

    .certification_border {
        border-width:2px;
        border-style:dashed;
        padding-top: 50px;
        padding-left: 25px;
        padding-right: 25px;
        padding-bottom: 140px;
    }

.issue{
    padding-top: 30px;
    font-size: 14px;
    color: black;
    float: left;
    word-spacing: 2px;
    text-indent: 20px;
    line-height: 1.2;
    font-style: inherit;
}
    .cert_para{
        padding-top: 20px;
        font-size: 14px;
        float: right;
        color: black;
        word-spacing: 2px;
        text-indent: 20px;
        padding-right: 100px;
        line-height: 1.2;
        font-style: inherit;
    }
.ani_image{
    float: right;
    padding-right: 100px;
}
    .red{
        float: right;
        text-indent: 75px;
        color: lightslategrey;
        text-shadow: 2px 1px orange;
    }
    .reds{
        float: right;
        text-indent: 0;
        color: lightslategrey;
        text-shadow: 2px 1px orange;
    }
.kabs_text{
    font-size: 12px;
    color: lightslategrey;
    text-shadow: 2px 1px orange;
}
    .good_character{
        padding-top: 20px;
        text-indent: 0;
    }

</style>


<body>
<div class="container-fluid certification_border">
    <div class="container ">
<div class="container-fluid">
    <div class="container gffsdg">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <strong class="kabs">KABS ANIMATION</strong> <img class="ani_image" src="image/ani.png" width="70" height="43"><br>

                <strong class="kabs_text">No 236c, Newpet, Near Daily Thanthi Office, Roundana Krishnagiri.</strong> <p class="red"><strong>Ready for</strong></p><br>
                <strong class="kabs_text">Krishnagiri - 635 001, Tamil Nadu, S.India</strong><p class="reds"><strong>Every Good Work</strong></p><br>
                <strong class="kabs_text">Phone :822 045 6017 Email:Admin@kabsanimation.com<</strong><br>
            </div>


        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="container">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="computer"><strong>COURSE COMPLETION CERTIFICATE</strong></div>
        </div>
    </div>
</div>

<p>This is to certify that
     <strong>{{$data['name']}} </strong>
    Register No. <strong>{{$data['regno']}}</strong>
    has join date of courese <strong>{{$data['joindate']}} </strong>
    student of this kabs animation class studied in has completed the course <strong>{{$data['courses']}} </strong>
    at the course Start Date<br> <strong>{{$data['startdate']}} </strong>
    and End date <strong>{{$data['enddate']}} </strong>
    at the during the <strong>{{$data['duration']}} </strong>course completed. <br>
<p class="good_character">His conduct and character were good.</p>
</p>
<p class="issue">Issue date:   <?php
    echo date("d/m/y");
    ?></p> <p class="cert_para">Signature</p>
        <br>


    </div>
</div>
</body>
</html>

