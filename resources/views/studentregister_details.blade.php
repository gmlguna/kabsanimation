@extends('layout.main')
@section('title', 'Autocad | Animation Center in Krishnagiri')
@section('keywords', 'Animation Center in Krishnagiri')
@section('description', 'Animation Center in Krishnagiri')
@section('content')

    <h2 class="student_subhead" style="background-color:lightslategrey;"> <div class="welcome"><strong class="welcomekbas"> Welcome to Kabs animation </strong>
            <div class="right_logout">
                <a class="logout" href="{{ url('/staff_registerdetails') }}"><strong>StaffDetails</strong></a>
                <a class="logout" href="{{ url('/studentregister_details') }}"><strong>StudentDetails</strong></a>
                <a class="logout" href="{{ url('/traningdetails') }}"><strong>TraningClass Details</strong></a>
                <a class="logout" href="{{ url('/empenquire') }}"><strong>EnquireDetails</strong></a>
                <a class="logout" href ="employe"><strong>Logout</strong></a>
            </div>
    </div>
        </h2>

    <div class="container table_size">
        <h6>Student Details</h6>
        <table bgcolor="#bdb76b" border ="8" class="table">
            <thead>
        <tr class="boldtable" bgcolor="#d3d3d3">
            <td>Register_No</td>
            <td>Name</td>
            <td>Dob</td>
            <td>Mobileno</td>
            <td>Email</td>
            <td>Gender</td>
            <td>Startdate</td>
            <td>Enddate</td>
            <td>Duration</td>
            <td>Joindate</td>
            <td>Courses</td>
            <td>Address</td>
            <td>Image_upload</td>
        </tr>
            </thead>
        @foreach ($users as $user)
            <tr>
                <td>{{ $user->regno }}</td>
                <td>{{ $user->name }}</td>
                <td>{{ $user->dob }}</td>
                <td>{{ $user->mobileno }}</td>
                <td>{{ $user->email }}</td>
                <td>{{ $user->gender }}</td>
                <td>{{ $user->startdate }}</td>
                <td>{{ $user->enddate }}</td>
                <td>{{ $user->duration }}</td>
                <td>{{ $user->joindate }}</td>
                <td>{{ $user->courses }}</td>
                <td>{{ $user->address }}</td>
                <td> <img src="storage/{{$user->image_upload}}" alt="" width="50" height="50"/> </td>
                {{--<td><a href = 'seleced/{{ $user->id }}'button class="button button5">Purchase</a></td>--}}

            </tr>
        @endforeach
    </table>
    </div>

    <h1>
        <table>
            <tr>
                <a href ="/student_register"><button class="buttons buttons2">New Register</button></a>
                <a href ="/student_delete"><button class="buttons buttons2">Delete</button></a>
                <a href ="/student_edit"><button class="buttons buttons2">update</button></a>
                {{--<a href ="/seleced"><button class="button button2">Select</button></a>--}}
                </td>
            </tr>
        </table>
    </h1>

@endsection