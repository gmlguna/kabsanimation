@extends('layout.main')
@section('title', 'Autocad | Animation Center in Krishnagiri')
@section('keywords', 'Animation Center in Krishnagiri')
@section('description', 'Animation Center in Krishnagiri')
@section('content')

    <h2 class="student_subhead" style="background-color:lightslategrey;"> <div class="welcome"><strong class="welcomekbas"> Welcome to Kabs animation </strong>
            <div class="right_logout">
                <a class="logout" href="{{ url('/admin_registerdetails') }}"><strong>StaffDetails</strong></a>
                <a class="logout" href="{{ url('/admin_studentdetails') }}"><strong>StudentDetails</strong></a>
                <a class="logout" href="{{ url('/admin_traningdetails') }}"><strong>TraningClass Details</strong></a>
                <a class="logout" href="{{ url('/enquridetails') }}"><strong>EnquireDetails</strong></a>
                <a class="logout" href ="admin"><strong>Logout</strong></a>
            </div>
        </div>
    </h2>

    <div class="container-fluid">
        <div class="container table_size">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <h6>Student Details</h6>
                    <div class="container table_size">
                        <table bgcolor="#bdb76b" border ="8" class="table">
                            <thead>
                            <tr class="boldtable" bgcolor="#d3d3d3">
                                <td>Register_No</td>
                                <td>Name</td>
                                <td>Dob</td>
                                <td>Email</td>
                                <td>Gender</td>
                                <td>Startdate</td>
                                <td>Enddate</td>
                                <td>Duration</td>
                                <td>Joindate</td>
                                <td>Courses</td>
                                <td>Address</td>
                                <td>Image_upload</td>
                            </tr>
                            </thead>
                            @foreach ($users as $user)
                                <tr>
                                    <td>{{ $user->regno }}</td>
                                    <td>{{ $user->name }}</td>
                                    <td>{{ $user->dob }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->gender }}</td>
                                    <td>{{ $user->startdate }}</td>
                                    <td>{{ $user->enddate }}</td>
                                    <td>{{ $user->duration }}</td>
                                    <td>{{ $user->joindate }}</td>
                                    <td>{{ $user->courses }}</td>
                                    <td>{{ $user->address }}</td>
                                    <td> <img src="storage/{{$user->image_upload}}" alt="" width="50" height="50"/> </td>
                                    {{--<td><a href = 'seleced/{{ $user->id }}'button class="button button5">Purchase</a></td>--}}

                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>



@endsection