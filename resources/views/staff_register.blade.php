@extends('layout.main')
@section('title', 'Autocad | Animation Center in Krishnagiri')
@section('keywords', 'Animation Center in Krishnagiri')
@section('description', 'Animation Center in Krishnagiri')
@section('content')

    <h2 class="student_subhead" style="background-color:lightslategrey;"> <div class="welcome"><strong class="welcomekbas"> Welcome to Kabs animation </strong>
            <div class="right_logout">
                <a class="logout" href="{{ url('/staff_registerdetails') }}"><strong>StaffDetails</strong></a>
                <a class="logout" href="{{ url('/studentregister_details') }}"><strong>StudentDetails</strong></a>
                <a class="logout" href="{{ url('/traningdetails') }}"><strong>TraningClass Details</strong></a>
                <a class="logout" href="{{ url('/empenquire') }}"><strong>EnquireDetails</strong></a>
                <a class="logout" href ="employe"><strong>Logout</strong></a>
            </div>
        </div>
    </h2>

    <h4>
        <center style="color:red;">
            @if(session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif
        </center>
    </h4>

    <form action="/staff_register" method="post" enctype="multipart/form-data">
        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

        <div class="container-fluid">
            <div class="container">
                <form method="post" action="#">
                    <div data-wow-delay=".1s" class="col-md-12 col-sm-12 col-xs-12 col-lg-12 padding_left_right_null">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 staff_register">
                            <img data-wow-delay=".10s" class="whicher_img wow zoomIn" src="{{URL::asset('image/sanpshot/6.jpg') }}" width="540"><br>
                        </div>

                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 ">
                            <h3 style="background-color:lightslategrey; text-align: center; text-shadow: 1px 0;">&nbsp;Register New Account</h3>

                            <div class="sizes"><strong>Enter The Name:</strong><span class="star-rating">*</span></div>
                            <fieldset class="{{ $errors->has('name') ? ' has-error' : '' }}">
                                <input type="text" id="name" name="name" placeholder="enter the Name..">
                                @if ($errors->has('name'))<span
                                        class="help-block error_font"><strong>{{ $errors->first('name') }}</strong></span>@endif

                            </fieldset>



                            <div class="sizes"><strong> Date OF Birth: </strong><span class="star-rating">*</span></div>
                            <fieldset class="{{ $errors->has('dob') ? ' has-error' : '' }}">
                                <input type="date" id="dob" name="dob">
                                @if ($errors->has('dob'))<span
                                        class="help-block error_font"><strong>{{ $errors->first('dob') }}</strong></span>@endif

                            </fieldset>


                            <div class="sizes"><strong>Mobile No:</strong><span class="star-rating">*</span></div>
                            <fieldset class="{{ $errors->has('mobileno') ? ' has-error' : '' }}">
                                <input type="text" id="mobileno" name="mobileno"
                                       placeholder="enter the  password.....">
                                @if ($errors->has('mobileno'))<span
                                        class="help-block error_font"><strong>{{ $errors->first('mobileno') }}</strong></span>@endif

                            </fieldset>


                            <div class="sizes"><strong>E-mail address:</strong><span
                                        class="star-rating">*</span></div>
                            <fieldset class="{{ $errors->has('email') ? ' has-error' : '' }}">
                                <input type="text" id="email" name="email"
                                       placeholder="enter the mobile number or email address.....">
                                @if ($errors->has('email'))<span
                                        class="help-block error_font"><strong>{{ $errors->first('email') }}</strong></span>@endif
                            </fieldset>

                            <div class="sizes"><strong>Gender:</strong><span class="star-rating">*</span></div>
                            <fieldset class="{{ $errors->has('gender') ? ' has-error' : '' }}">
                                <select id="gender" name="gender">
                                    <option value="">select the Gender</option>
                                    <option value="male">Male</option>
                                    <option value="female">Female</option>
                                    <option value="other">Other</option>
                                </select>
                                @if ($errors->has('gender'))<span
                                        class="help-block error_font"><strong>{{ $errors->first('gender') }}</strong></span>@endif
                            </fieldset>


                            <div class="sizes"><strong>Join Date:</strong><span class="star-rating">*</span>
                            </div>
                            <fieldset class="{{ $errors->has('joindate') ? ' has-error' : '' }}">
                                <input type="date" id="joindate" name="joindate">
                                @if ($errors->has('joindate'))<span
                                        class="help-block error_font"><strong>{{ $errors->first('joindate') }}</strong></span>@endif
                            </fieldset>

                            <div class="sizes"><strong>Permanent Address:</strong><span class="star-rating">*</span>
                            </div>
                            <fieldset class="{{ $errors->has('address') ? ' has-error' : '' }}">
                                <textarea id="address" name="address" placeholder="Write permanent address...."
                                          style="height:80px"></textarea>
                                @if ($errors->has('address'))<span
                                        class="help-block error_font"><strong>{{ $errors->first('address') }}</strong></span>@endif
                            </fieldset>

                            <div class="sizes"><strong>Image upload</strong><span class="star-rating">*</span>
                                {{csrf_field()}}
                                <input type="file" name="image_upload"/>
                            </div>

                            <div class="button_submit">
                                <input type="submit" value="Submit">&nbsp;
                                <input type="reset" value="Clear">&nbsp;
                            </div>

                        </div>


                    </div>
                </form>
            </div>
        </div>

@endsection