
@extends('layout.main')
@section('title', 'Bespoke | Animation Center in Krishnagiri')
@section('keywords', 'Animation Center in Krishnagiri')
@section('description', 'Animation Center in Krishnagiri')
@section('content')
    <form action="/home" method="post">
        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
        <div class="container-fluid log_form common_bg_styless">
            <div class="container">
                <h4>
                    <center style="color:red;">
                        @if(session()->has('message'))
                            <div class="alert alert-success">
                                {{ session()->get('message') }}
                            </div>
                        @endif
                    </center>
                </h4>
                <div class="enq"><strong>Enquire now!</strong></div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <select id="location" class="input" name="location">
                            <option value="location">Choose Location</option>
                            <option value="krishnagiri">Krishnagiri</option>
                            <option value="vellore">Vellore</option>
                            <option value="tirupattur">Tirupattur</option>
                            <option value="hosur">hosur</option>
                            <option value="chennai">chennai</option>
                        </select>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 list">
                        <fieldset class="{{ $errors->has('company') ? ' has-error' : '' }}">
                            <input type="text" id="company" name="company" class="input" placeholder="enter the company..">
                            @if ($errors->has('company'))<span
                                    class="help-block error_font"><strong>{{ $errors->first('company') }}</strong></span>@endif
                        </fieldset>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 list">
                        <fieldset class="{{ $errors->has('name') ? ' has-error' : '' }}">
                            <input type="text" id="name" name="name" class="input" placeholder="enter the name..">
                            @if ($errors->has('name'))<span
                                    class="help-block error_font"><strong>{{ $errors->first('name') }}</strong></span>@endif
                        </fieldset>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 list">
                        <fieldset class="{{ $errors->has('email') ? ' has-error' : '' }}">
                            <input type="text" id="email" name="email" class="input" placeholder="enter the email..">
                            @if ($errors->has('email'))<span
                                    class="help-block error_font"><strong>{{ $errors->first('email') }}</strong></span>@endif
                        </fieldset>
                    </div>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 list">
                        <fieldset class="{{ $errors->has('phone') ? ' has-error' : '' }}">
                            <input type="text" id="phone" name="phone" class="input" placeholder="enter the phone..">
                            @if ($errors->has('phone'))<span
                                    class="help-block error_font"><strong>{{ $errors->first('phone') }}</strong></span>@endif
                        </fieldset>
                        <br>
                        <br>
                        <input type="text" id="date" name="date" class="input" readonly value="<?php echo date("d / m / y");?>">
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 list">
                        <textarea name="yourmsg"  placeholder="Your message*" class="input" required></textarea>
                        <div class="special_offers">
                            <input type="checkbox" name="checkbox" value="">
                            I would like to get news about courses and special offers</div>
                        <button input type="submit" name="submit" class="btn">ENQUIRE NOW</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
@endsection