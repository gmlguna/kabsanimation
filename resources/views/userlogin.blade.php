@extends('layout.main')
@section('title', 'Autocad | Animation Center in Krishnagiri')
@section('keywords', 'Animation Center in Krishnagiri')
@section('description', 'Animation Center in Krishnagiri')
@section('content')


    {{--@if(isset($data))--}}
    {{--{{$data}}--}}
    {{--@endif--}}
    <form action="/userlogin" method="post">
        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

        <div class="container-fluid">
            <div class="container">
                <form method="post" action="#">
                    <div data-wow-delay=".1s"
                         class="col-md-12 col-sm-12 col-xs-12 col-lg-12 fpr padding_left_right_null">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 ">
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 staff_form">
                            <h3 class="logincontent"
                                style="background-color:#383d46; text-align: center; text-shadow: 1px 0;">Log In</h3>
                            <div class="sizeses"><strong>Register No:</strong><span class="star-rating">*</span></div>
                            <fieldset class="{{ $errors->has('regno') ? ' has-error' : '' }}">
                                <input class="input_color" types="text" id="regno" name="regno"
                                       placeholder="Register No">
                                @if ($errors->has('regno'))<span
                                        class="help-block error_font"><strong>{{ $errors->first('regno') }}</strong></span>@endif
                                <h5>
                                    <center style="color:red;">
                                        @if(session()->has('message'))
                                            <div class="alert alert-success">
                                                {{ session()->get('message') }}
                                            </div>
                                        @endif
                                    </center>

                                </h5>
                            </fieldset>
                            <div class="sizeses"><strong>Password:</strong><span class="star-rating">*</span></div>
                            <fieldset class="{{ $errors->has('dob') ? ' has-error' : '' }}">
                                <input class="input_color" type="password" id="dob" name="dob"
                                       placeholder="Password">
                                @if ($errors->has('dob'))<span
                                        class="help-block error_font"><strong>{{ $errors->first('dob') }}</strong></span>@endif

                            </fieldset>


                            <div class="button_submit">
                                <input type="submit" value="Submit">&nbsp;
                            </div>

                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 ">
                        </div>
                    </div>
                </form>
            </div>
        </div>


@endsection