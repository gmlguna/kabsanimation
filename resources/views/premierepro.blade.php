
@extends('layout.main')
@section('title', 'Premierepro | Animation Center in Krishnagiri')
@section('keywords', 'Animation Center in Krishnagiri')
@section('description', 'Animation Center in Krishnagiri')
@section('content')

    <div class="container-fluid prremierepro_home common_bg_style">
        <h4>
            <center style="color:red;">
                @if(session()->has('message'))
                    <div class="alert alert-success">
                        {{ session()->get('message') }}
                    </div>
                @endif
            </center>
        </h4>
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 adobe_after">
                    <strong>Adobe Premiere <br> Pro Courses</strong>
                    <h5 class="strong"><strong>Adobe Premiere Pro is a widely-used video and multimedia editing and post
                            production software.</strong>
                    </h5>
                    <h5 class="strong">It is highly recommended by the industry experts in film-making and broadcasting. At
                        Kabs animation Class you can learn how to use Adobe Premiere Pro to produce videos that have a professional
                        look.
                    </h5>
                    <h5 class="strong"><strong>18-month Free class retake included.</strong>
                    </h5>
                    <img alt="adobe" title="adobe" src="{{URL::asset('image/after/adobe.jpg') }}" width="150" height="50">
                </div>
                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                    <div class="after_video">
                        <iframe class="iframe" src="https://www.youtube.com/embed/X8mvcE1HwLk"></iframe>
                    </div>
                </div>
            </div>
        </div>


        <div class="container-fluid ss_sanpshot">
            <div class="container">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 training_courses button">
                        <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null" href="{{ url('/') }}">
                            <button type="button" class="moredetails" class="moredetails-arrow-down">MORE DETAILS</button>
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 training_courses">
                        <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null" href="{{ url('/') }}">
                            <button type="button" class="charwith">CHAT WITH US</button>
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 training_courses">
                        <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null" href="{{ url('/') }}">
                            <button type="button" class="phonenumber">8220456017</button>
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 training_courses">
                        <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null" href="{{ url('/enquri') }}">
                            <button type="button" class="enquire">ENQUIRE NOW</button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 choose_your">
                    <div class="descript"><strong>Class Snapshots:</strong></div>
                    <img class="adobe" src="{{URL::asset('image/sanpshot/1.jpg') }}"><br>
                    <img class="adobe" src="{{URL::asset('image/sanpshot/2.jpg') }}"><br>
                    <img class="adobe" src="{{URL::asset('image/sanpshot/3.jpg') }}"><br>
                    <img class="adobe" src="{{URL::asset('image/sanpshot/4.jpg') }}"><br>
                    <img class="adobe" src="{{URL::asset('image/sanpshot/5.jpg') }}"><br>
                    <img class="adobe" src="{{URL::asset('image/sanpshot/6.jpg') }}"><br>
                    <img class="adobe" src="{{URL::asset('image/sanpshot/7.jpg') }}"><br>
                    <img class="adobe" src="{{URL::asset('image/sanpshot/3.jpg') }}"><br>
                </div>


                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 course_information">
                    <h2><strong> Course Information:</strong></h2>
                    <div class="description"><strong>Adobe After Effects Course Description</strong></div>
                    <div class="course_information_paragraph">Adobe Premiere Pro is a widely-used video and multimedia
                        editing and post production software, highly recommended by the industry experts in film-making and
                        broadcasting for producing powerful, compelling content. Working at HD, 4K, 8K and beyond, it mixes
                        frame rates and video formats in the same timeline. It works with footage from broadcast cameras,
                        high-end digital device, mobile device, DSLR, action camera or any source. Add audio and video
                        effects, titles, and export it to any screen or format, integrating with Photoshop, Illustrator &
                        After Effects.
                    </div>

                    <div class="description"><strong>What will you learn?</strong></div>
                    <div class="course_information_paragraph"><img src="{{URL::asset('image/cb.jpg') }}" width="20" height="20"> This course
                        will help students master the art of editing and interface design.
                    </div>

                    <div class="course_information_paragraph"><img src="{{URL::asset('image/cb.jpg') }}" width="20" height="20"> It will also
                        teach you to edit video clips, create digital movies for professional quality output, assemble
                        video, audio, and still images.
                    </div>

                    <div class="course_information_paragraph"><img src="{{URL::asset('image/cb.jpg') }}" width="20" height="20"> With Kabs animation
                        Class’ Adobe Premiere Pro course you can apply your visual skills and get a command over video
                        editing.
                    </div>
                    <div class="course_information_paragraph"><img src="{{URL::asset('image/cb.jpg') }}" width="20" height="20"> You will learn
                        video production techniques including combining video, audio, and image-stills, timelines,
                        assembling video clips, controlling sequences and enhancing them with transitions such as wipes and
                        fades, introducing title sequences, fading and combining multiple audio tracks, and outputting to
                        different formats for high, medium and lower quality requirements.
                    </div>

                    <div class="course_information_paragraph"><img src="{{URL::asset('image/cb.jpg') }}" width="20" height="20"> In Adobe
                        Premiere Pro courses you will be learning how to do project management, image processing filters,
                        motion control, compositing and superimposing, precision audio editing, compression and optimization
                        of video, and dynamic integration with other Adobe applications.
                    </div>

                    <div class="course_information_paragraph"><img src="{{URL::asset('image/cb.jpg') }}" width="20" height="20"> Students will
                        also be trained to use features of transparency, Alpha and Luma keying, Chroma keying, image
                        adjustment and distort filters, split edits, audio mixer, etc.
                    </div>
                    <div class="description"><strong>Blended Learning</strong></div>
                    <div class="course_information_paragraph">It’s the best opportunity to get the most out of your learning
                        experience while blending technology with classroom instructions. We supply:
                        <ul class="h5 strong">
                            <li>training videos,</li>
                            <li>notes and/or</li>
                            <li>reference texts.</li>
                        </ul>
                    </div>

                    <h2 class="learn"><strong> How You Want To Learn</strong></h2>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 course_information">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <img class="right" src="{{URL::asset('image/cb.jpg') }}" width="20" height="20">
                            <div class="course_information_paragraphs"><strong>Individual</strong></div>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 course_information">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <img class="right" src="{{URL::asset('image/cb.jpg') }}" width="20" height="20">
                            <div class="course_information_paragraphs"><strong>Customis<br>ed</strong></div>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 course_information">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <img class="right" src="{{URL::asset('image/cb.jpg') }}" width="20" height="20">
                            <div class="course_information_paragraphs"><strong>Classroom</strong></div>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 course_information">
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                            <img class="right" src="{{URL::asset('image/cb.jpg') }}" width="20" height="20">
                            <div class="course_information_paragraphs"><strong> Live-Online </strong></div>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <h1><strong>Still Not Convinced?</strong></h1>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="still"><strong>18-Month Free<br>Class Retake</strong>
                        </div>
                        <h>If you have any gaps in your knowledge or want to refresh your skills, you are more than
                            welcome to come back  live online class free of charge up to 18 months after
                            you have taken the class.
                        </h>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="still"><strong>Money-Back <br>
                                Guarantee</strong>
                        </div>
                        <h>If you don’t absolutely LOVE your class, we’ll give you a full refund! Let us know on the
                            FIRST day of your training if something isn’t quite right and give us a chance to fix it or
                            give you your money back.
                        </h>
                    </div>


                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="still"><strong>Lower Price<br>Guarantee</strong>
                        </div>
                        <h>We think our prices are pretty fair but we won’t be beaten on our fee. We’ll match and
                            discount by 10% any like-for-like Training course price.
                        </h>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="still"><strong>Experienced<br>Instructors</strong>
                        </div>
                        <h>Equipped with years of industry experience our instructors will assure a successful leap in
                            your knowledge, improvement and preparation.
                        </h>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="two"><strong> “The investment in knowledge pays the best interests.”</strong></div>
                            <br>
                            <p class="frank">~ Benjamin Franklin</p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="container-fluid ss_sanpshot">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 training_courses button">
                    <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null" href="{{ url('/') }}">
                        <button type="button" class="moredetails" class="moredetails-arrow-down">MORE DETAILS</button>
                    </a>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 training_courses">
                    <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null" href="{{ url('/') }}">
                        <button type="button" class="charwith">CHAT WITH US</button>
                    </a>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 training_courses">
                    <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null" href="{{ url('/') }}">
                        <button type="button" class="phonenumber">8220456017</button>
                    </a>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 training_courses">
                    <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null" href="{{ url('/enquri') }}">
                        <button type="button" class="enquire">ENQUIRENOW</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid course_time">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 course_times">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null">
                        <h3 class="course-time"><span
                                    style="color: #ffffff;">Course<br> Times:</span>
                        </h3>
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 course_times">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null">
                        <img src="{{URL::asset('image/course1.png') }}" width="70" height="70">
                        <br>
                        <br>
                        <p style="text-align: center;">
                            <span style="color: #00969d; margin-top: 5px;">9:00 – 11:00</span><br>
                            <span style="color: #ffffff;">Course</span>
                        </p>
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 course_times">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null">
                        <img src="{{URL::asset('image/course2.jpg') }}" width="70" height="70">
                        <br>
                        <br>
                        <p style="text-align: center;">
                            <span style="color: #00969d; margin-top: 5px;">11:00 – 11:15</span><br>
                            <span style="color: #ffffff;">Break</span>
                        </p>
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 course_times">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null">
                        <img src="{{URL::asset('image/course3.png') }}" width="70" height="70">
                        <br>
                        <br>
                        <p style="text-align: center;">
                            <span style="color: #00969d; margin-top: 5px;">11:15 – 13:00</span><br>
                            <span style="color: #ffffff;">Course</span>
                        </p>
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 course_times">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null">
                        <img src="{{URL::asset('image/course4.png') }}" width="70" height="70">
                        <br>
                        <br>
                        <p style="text-align: center;">
                            <span style="color: #00969d; margin-top: 5px;">13:00 – 14:00</span><br>
                            <span style="color: #ffffff;">Break</span>
                        </p>
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 course_times">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null">
                        <img src="{{URL::asset('image/course5.png') }}" width="70" height="70">
                        <br>
                        <br>
                        <p style="text-align: center;">
                            <span style="color: #00969d; margin-top: 5px;">14:00 – 16:30</span><br>
                            <span style="color: #ffffff;">Course</span>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <h2 class="course_leaves"><strong>Course Levels</strong></h2>
                    <div class="single_course"> SINGLE COURSES</div>

                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <img src="{{URL::asset('image/single1.jpg') }}" width="45" height="45">
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                            <div class="yellow"><strong>Yellow Belt:<br>101</strong></div>
                            <div class="package">Beginners</div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <img src="{{URL::asset('image/single2.jpg') }}" width="45" height="45">
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                            <div class="yellow"><strong>Green Belt:<br>201</strong></div>
                            <div class="package">Intermediate</div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <img src="{{URL::asset('image/single3.jpg') }}" width="45" height="45">
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                            <div class="yellow"><strong>Black Belt:<br>301</strong></div>
                            <div class="package">Advanced</div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <img src="{{URL::asset('image/single4.jpg') }}" width="45" height="45">
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                            <div class="yellow"><strong>Certified: 401</strong></div>
                            <div class="package">Expert</div>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="single_course">PACKAGED COURSES</div>
                        </div>
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <img src="{{URL::asset('image/single5.jpg') }}" width="45" height="45">
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                            <div class="yellow"><strong>Zero to Hero</strong></div>
                            <div class="packagese">Complete<br>
                                courses 101,<br>
                                201 and 301 in a<br>
                                combined and<br>
                                discounted<br>
                                package.
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <img src="{{URL::asset('image/single6.jpg') }}" width="45" height="45">
                        </div>

                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                            <div class="yellow"><strong>Ultimate<br>Mastery</strong></div>
                            <div class="packagese">Complete<br>
                                courses 101,<br>
                                201, 301 and<br>
                                401 in a<br>
                                combined and<br>
                                discounted<br>
                                package.
                            </div>
                        </div>
                    </div>

                </div>


                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 course_information">
                    <form action="/home" method="post">
                        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                        <h2 class="enquirenow"><strong> That’s us. That’s you. Enquire now!</strong></h2>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <fieldset class="{{ $errors->has('location') ? ' has-error' : '' }}">
                                <select id="location" name="location">
                                    <option value="location">Choose Location</option>
                                    <option value="krishnagiri">Krishnagiri</option>
                                    <option value="vellore">Vellore</option>
                                    <option value="tirupattur">Tirupattur</option>
                                    <option value="hosur">hosur</option>
                                    <option value="chennai">chennai</option>
                                </select>
                                @if ($errors->has('location'))<span
                                        class="help-block error_font"><strong>{{ $errors->first('location') }}</strong></span>@endif
                            </fieldset>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 list">
                            <fieldset class="{{ $errors->has('company') ? ' has-error' : '' }}">
                                <input type="text" id="company" name="company" placeholder="enter the company..">
                                @if ($errors->has('company'))<span
                                        class="help-block error_font"><strong>{{ $errors->first('company') }}</strong></span>@endif
                            </fieldset>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 list">
                            <fieldset class="{{ $errors->has('name') ? ' has-error' : '' }}">
                                <input type="text" id="name" name="name" placeholder="enter the name..">
                                @if ($errors->has('name'))<span
                                        class="help-block error_font"><strong>{{ $errors->first('name') }}</strong></span>@endif
                            </fieldset>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 list">
                            <fieldset class="{{ $errors->has('email') ? ' has-error' : '' }}">
                                <input type="text" id="email" name="email" placeholder="enter the email..">
                                @if ($errors->has('email'))<span
                                        class="help-block error_font"><strong>{{ $errors->first('email') }}</strong></span>@endif
                            </fieldset>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 list">
                            <fieldset class="{{ $errors->has('phone') ? ' has-error' : '' }}">
                                <input type="text" id="phone" name="phone" placeholder="enter the phone..">
                                @if ($errors->has('phone'))<span
                                        class="help-block error_font"><strong>{{ $errors->first('phone') }}</strong></span>@endif
                            </fieldset>
                            <br>
                            <br>
                            <input type="text" id="date" name="date" readonly value="<?php echo date("d / m / y");?>">
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 list">
                            <textarea name="yourmsg" placeholder="Your message*" required></textarea>
                            <div class="special_offers">
                                <input type="checkbox" name="checkbox" value="">
                                I would like to get news about courses and special offers</div>
                            <button input type="submit" name="submit" class="btn">ENQUIRE NOW</button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
    <div class="container-fluid ss_sanpshot">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 training_courses button">
                    <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null" href="{{ url('/') }}">
                        <button type="button" class="moredetails" class="moredetails-arrow-down">MORE DETAILS</button>
                    </a>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 training_courses">
                    <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null" href="{{ url('/') }}">
                        <button type="button" class="charwith">CHAT WITH US</button>
                    </a>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 training_courses">
                    <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null" href="{{ url('/') }}">
                        <button type="button" class="phonenumber">8220456017</button>
                    </a>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 training_courses">
                    <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null" href="{{ url('/enquri') }}">
                        <button type="button" class="enquire">ENQUIRENOW</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection