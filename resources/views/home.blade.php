@extends('layout.main')
@section('title', 'Home | Animation Center in Krishnagiri')
@section('keywords', 'Animation Center in Krishnagiri')
@section('description', 'Animation Center in Krishnagiri')
@section('content')

    <div class="container-fluid sanpshot home_bg common_bg_style">
        <h4>
            <center style="color:red;">
                @if(session()->has('message'))
                    <div class="alert alert-success">
                        {{ session()->get('message') }}
                    </div>
                @endif
            </center>
        </h4>

        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="container">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 professionals">
                    <h2 data-wow-delay=".3s" class="wow bounceInLeft"><strong>Real Training for Real People by Real Professionals!</strong></h2>
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_bottom_ten_em">
                    <a class="col-lg-5 col-md-5 col-sm-5 col-xs-12 browse_course " href="{{ url('/') }}">
                        <button data-wow-delay=".5s" class="browser courses_button wow bounceInUp">BROWSES COURSES</button>
                    </a>
                        <img class="popup popupimg"  id="myBtn" src="image/news1.gif"  style="width:30%;max-width:115px">
                        <div id="myModal" class="modal">
                            <div class="modal-content">
                                <span class="close">&times;</span>
                                <img id="myImg" src="image/popup.png"  style="width:100%; height:60%; max-width:100%">
                            </div>
                        </div>
                        <script>

                            var modal = document.getElementById('myModal');
                            var btn = document.getElementById("myBtn");
                            var span = document.getElementsByClassName("close")[0];

                            btn.onclick = function() {
                                modal.style.display = "block";
                            }

                            span.onclick = function() {
                                modal.style.display = "none";
                            }

                            window.onclick = function(event) {
                                if (event.target == modal) {
                                    modal.style.display = "none";
                                }
                            }
                        </script>


                </div>
            </div>
    </div>
    </div>



    <div class="container-fluid sanpshot home_bg_one common_bg_style">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 course">
                <h2>Choose From Our Top Training Courses!</h2>
            </div>
        </div>
        <div class="container-fluid">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null">
                <div class="container">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null courses_details">
                        <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 training_courses padding_left_right_null">
                            <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null" href="{{ url('/photoshop') }}">
                                <img data-wow-delay=".2s" class="wow zoomIn" src="{{URL::asset('image/home_image/adobecer.jpg') }}">
                                <div class="absolute">Photoshop</div>
                            </a>

                        </div>
                        <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 training_courses padding_left_right_null">
                            <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null" href="{{ url('/indesign') }}">
                                <img data-wow-delay=".2s" class="wow zoomIn" src="{{URL::asset('image/home_image/icons_80-1.jpg') }}">
                                <div class="absolute">InDesign</div>
                            </a>
                        </div>
                        <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 training_courses padding_left_right_null">
                            <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null" href="{{ url('/illustrator') }}">
                                <img data-wow-delay=".2s" class="wow zoomIn" src="{{URL::asset('image/home_image/icons_56-1.jpg') }}">
                                <div class="absolute">Illustrator</div>
                            </a>

                        </div>
                        <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 training_courses padding_left_right_null">
                            <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null" href="{{ url('/after_effects') }}">
                                <img data-wow-delay=".2s" class="wow zoomIn" src="{{URL::asset('image/home_image/icons_47.jpg') }}">
                                <div class="absolute">AfterEffects</div>
                            </a>

                        </div>
                        <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 training_courses padding_left_right_null">
                            <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null" href="{{ url('/premierepro') }}">
                                <img data-wow-delay=".2s" class="wow zoomIn" src="{{URL::asset('image/home_image/icons_51.jpg') }}">
                                <div class="absolute">premierePro</div>
                            </a>

                        </div>
                        <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 training_courses padding_left_right_null">
                            <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null" href="{{ url('/autocad') }}">
                                <img data-wow-delay=".2s" class="wow zoomIn" src="{{URL::asset('image/home_image/icons_57.jpg') }}">
                                <div class="absolute">AutoCAD</div>
                            </a>

                        </div>
                        <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 training_courses padding_left_right_null">
                            <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null" href=" {{ url('/revit') }}">
                                <img data-wow-delay=".2s" class="wow zoomIn" src="{{URL::asset('image/home_image/icons_39-1.jpg') }}">
                                <div class="absolute">Revit</div>
                            </a>

                        </div>
                        <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 training_courses padding_left_right_null">
                            <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null" href=" {{ url('/dsmax') }}">
                                <img data-wow-delay=".2s" class="wow zoomIn" src="{{URL::asset('image/home_image/icons_05.jpg') }}">
                                <div class="absolute">3dsMax</div>
                            </a>

                        </div>
                        <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 training_courses padding_left_right_null">
                            <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null" href=" {{ url('/cinema') }}">
                                <img data-wow-delay=".2s" class="wow zoomIn" src="{{URL::asset('image/home_image/icons_101-1.jpg') }}">
                                <div class="absolute">Cinema4D</div>
                            </a>

                        </div>
                        <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 training_courses padding_left_right_null">
                            <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null" href="{{ url('/uxdesign') }}">
                                <img data-wow-delay=".2s" class="wow zoomIn" src="{{URL::asset('image/home_image/icons_114.jpg') }}">
                                <div class="absolute">UxDesign</div>
                            </a>

                        </div>
                        <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 training_courses padding_left_right_null">
                            <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null" href="{{ url('/maya') }}">
                                <img data-wow-delay=".2s" class="wow zoomIn" src="{{URL::asset('image/home_image/icons_28-2.jpg') }}">
                                <div class="absolute">Maya</div>
                            </a>

                        </div>
                        <div class="col-lg-2 col-md-4 col-sm-6 col-xs-12 training_courses padding_left_right_null">
                            <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null" href="{{ url('/creativecloud') }}">
                                <img data-wow-delay=".2s" class="wow zoomIn" src="{{URL::asset('image/home_image/icons_59.jpg') }}">
                                <div class="absolute">HTML</div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                <a href="{{ url('/') }}">
                    <button data-wow-delay=".3s" class="browser courses_button wow shake">BROWSES ALL COURSES</button>
                </a>
            </div>
        </div>
    </div>

    <div class="container-fluid indival">
        <div class="container">
            <div data-wow-delay=".3s" class="col-lg-6 col-md-6 col-sm-12 col-xs-12 wow bounceInLeft">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 courses_img">
                    <img src="{{URL::asset('image/training/Individual.jpg') }}">
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 courses_img ">
                    <h6><strong>Individual Training</strong></h6>
                    <h5> Do you want to tone up your </h5>
                    <h5> skills? Launch a new career </h5>
                    <h5> or start your own business? </h5>
                    <h5> We can help you.</h5>
                    {{--<a href="#contact"><h5><font color="#daa520"> Click here </font></h5></a>--}}
                </div>
            </div>

            <div data-wow-delay=".3s" class="col-lg-6 col-md-6 col-sm-12 col-xs-12 wow bounceInRight">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 courses_img">
                    <img src="{{URL::asset('image/training/Corporate.jpg') }}">
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 courses_img">
                    <h6><strong>Corporate Training</strong></h6>
                    <h5> Send us your Design & </h5>
                    <h5> Development teams to </h5>
                    <h5> unleash their full potential </h5>
                    <h5> and receive great group </h5>
                    <h5> discounts.
                        {{--<a href="#contact"> <span style="color:#daa520"> Learn how here </span> </a>--}}
                    </h5>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid indival">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 academy_class">
                    <h2 data-wow-delay=".1s" class="wow animate fadeIn" >KABS ANIMATION CLASS</h2>
                    <div data-wow-delay=".2s" class="three animate fadeIn">
                        <strong>Market Leaders in Training Courses for Designers and Developers</strong>
                    </div>
                    <p class="wow animate fadeIn" data-wow-delay=".3s">Our training is specifically developed to keep giving you the edge in your career or business.</p>
                    <p class="wow animate fadeIn" data-wow-delay=".4s">This is what we like to call “the how and the now!”</p>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid ss_sanpshot">
        <div class="container">
            <h1 class="wow animate fadeIn" data-wow-delay=".2s"><strong>Class Snapshots</strong></h1>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 training_courses padding_left_right_null">
                    <a data-wow-delay=".2s" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 wow bounceInDown padding_left_right_null" href="{{ url('/') }}">
                        <img src="{{URL::asset('image/sanpshot/1.jpg') }}">
                    </a>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 training_courses padding_left_right_null">
                    <a data-wow-delay=".2s" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 wow bounceInDown  padding_left_right_null" href="{{ url('/') }}">
                        <img src="{{URL::asset('image/sanpshot/2.jpg') }}">
                    </a>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 training_courses padding_left_right_null">
                    <a data-wow-delay=".2s" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 wow bounceInDown   padding_left_right_null" href="{{ url('/') }}">
                        <img src="{{URL::asset('image/sanpshot/3.jpg') }}">
                    </a>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 training_courses padding_left_right_null">
                    <a data-wow-delay=".2s" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 wow bounceInDown  padding_left_right_null" href="{{ url('/') }}">
                        <img src="{{URL::asset('image/sanpshot/4.jpg') }}">
                    </a>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 training_courses padding_left_right_null">
                    <a data-wow-delay=".2s" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 wow bounceInUp padding_left_right_null" href="{{ url('/') }}">
                        <img src="{{URL::asset('image/sanpshot/5.jpg') }}">
                    </a>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 training_courses padding_left_right_null">
                    <a data-wow-delay=".2s" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 wow bounceInUp padding_left_right_null" href="{{ url('/') }}">
                        <img src="{{URL::asset('image/sanpshot/6.jpg') }}">

                    </a>

                </div>
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 training_courses padding_left_right_null">
                    <a data-wow-delay=".2s" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 wow bounceInUp padding_left_right_null" href="{{ url('/') }}">
                        <img src="{{URL::asset('image/sanpshot/7.jpg') }}">
                    </a>

                </div>
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 training_courses padding_left_right_null">
                    <a data-wow-delay=".2s" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 wow bounceInUp padding_left_right_null" href="{{ url('/') }}">
                        <img src="{{URL::asset('image/sanpshot/8.jpg') }}">
                    </a>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid indival">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 whichever">
                    <h1 class="wow animate fadeIn" data-wow-delay=".1s" ><strong>Whichever Way You Want To Learn</strong></h1>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div data-wow-delay=".1s" class="course_information_paragraphs wow shake "><img class="right" src="{{URL::asset('image/cb.jpg') }}" width="20" height="20"><strong>Classroom</strong>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div data-wow-delay=".1s" class="course_information_paragraphs wow shake "><img class="right" src="{{URL::asset('image/cb.jpg') }}" width="20" height="20"><strong>Customised</strong>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div data-wow-delay=".1s" class="course_information_paragraphs wow shake "><img class="right" src="{{URL::asset('image/cb.jpg') }}" width="20" height="20"><strong>Live-Online</strong>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                    <div data-wow-delay=".1s" class="course_information_paragraphs wow shake "><img class="right" src="{{URL::asset('image/cb.jpg') }}" width="20" height="20">
                        <strong>Individual / Corporate</strong></div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid indival">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 after_your">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <img data-wow-delay=".10s" class="whicher_img wow zoomIn" src="{{URL::asset('image/whicher.jpg') }}" width="659" height="463">
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="careerss">
                        <div data-wow-delay=".1s" class="whichever_para wow animate fadeIn"><strong>
                                AFTER OUR COURSES YOU WILL:</strong>
                        </div>
                        <ul>
                            <li class="wow bounceInRight" data-wow-delay=".1s">Improve your professional skill set</li>
                            <li class="wow bounceInRight" data-wow-delay=".2s">Build and grow your portfolio</li>
                            <li class="wow bounceInRight" data-wow-delay=".3s">Solve complex challenges people typically face</li>
                            <li class="wow bounceInRight" data-wow-delay=".4s">Get great tips and tricks from practicing, professional instructors</li>
                            <li class="wow bounceInRight" data-wow-delay=".5s">Increase your confidence to develop your own business</li>
                            <li class="wow bounceInRight" data-wow-delay=".6s"><strong>Become an accredited specialist</strong> (ace, aca, acu, acp, etc). Read more here</li>
                            <li class="wow bounceInRight" data-wow-delay=".7s"><strong>Get digital certificates</strong><br>(Great for uploading to your LinkedIn profile and other social media)</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid indival">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 stills">
                    <h1 class="wow animated fadeIn" data-wow-delay=".1s"><strong>Still Not Convinced?</strong></h1>
                </div>
            </div>

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_top_one_em padding_left_right_null">
                    <div data-wow-delay=".1s" class="col-lg-6 col-md-6 col-sm-6 col-xs-12 wow animated zoomIn">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                            <div class="still"><strong>18-Month Free<br>Class Retake</strong>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                            <h>If you have any gaps in your knowledge or want to refresh your skills, you are more than
                                welcome to come back and retake the live online class free of charge up to 18 months after
                                you have taken the class.
                            </h>
                        </div>
                    </div>
                    <div data-wow-delay=".1s" class="col-lg-6 col-md-6 col-sm-6 col-xs-12 wow animated zoomIn">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                            <div class="still"><strong>Money-Back <br>
                                    Guarantee</strong>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                            <h>If you don’t absolutely LOVE your class, we’ll give you a full refund! Let us know on the
                                FIRST day of your training if something isn’t quite right and give us a chance to fix it or
                                give you your money back.
                            </h>
                        </div>
                    </div>
                </div>

                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padd_top_one_em padding_left_right_null">
                    <div data-wow-delay=".1s" class="col-lg-6 col-md-6 col-sm-6 col-xs-12 wow animated zoomIn">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                            <div class="still"><strong>Lower Price<br>Guarantee</strong>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                            <h>We think our prices are pretty fair but we won’t be beaten on our fee. We’ll match and
                                discount by 10% any like-for-like Training course price.
                            </h>
                        </div>
                    </div>
                    <div data-wow-delay=".1s" class="col-lg-6 col-md-6 col-sm-6 col-xs-12 wow animated zoomIn">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                            <div class="still"><strong>Experienced<br>Instructors</strong>
                            </div>
                        </div>
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 ">
                            <h>Equipped with years of industry experience our instructors will assure a successful leap in
                                your knowledge, improvement and preparation.
                            </h>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null">
                    <div data-wow-delay=".2s" class="two wow bounceInRight"><strong>“The investment in knowledge pays the best interests.”</strong></div>
                    <br>
                    <p data-wow-delay=".2s" class="two wow bounceInLeft">~ Benjamin Franklin</p>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 course">
                <h2 data-wow-delay=".1s" class="wow animated fadeIn" >That’s us. Now it’s up to you. Enquire now!</h2>
            </div>
        </div>
    </div>


    <div class="container-fluid form_image common_bg_style">
        <div class="container">
            <form action="/home" method="post">
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <div data-wow-delay=".1s" class ="col-md-12 col-sm-12 col-xs-12 col-lg-12 padding_left_right_null wow animated zoomIn">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <fieldset class="{{ $errors->has('location') ? ' has-error' : '' }}">
                                <select id="location" name="location">
                                    <option value="location">Choose Location</option>
                                    <option value="krishnagiri">Krishnagiri</option>
                                    <option value="vellore">Vellore</option>
                                    <option value="tirupattur">Tirupattur</option>
                                    <option value="hosur">hosur</option>
                                    <option value="chennai">chennai</option>
                                </select>
                                @if ($errors->has('location'))<span
                                        class="help-block error_font"><strong>{{ $errors->first('location') }}</strong></span>@endif
                            </fieldset>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 list">
                            <fieldset class="{{ $errors->has('company') ? ' has-error' : '' }}">
                                <input type="text" id="company" name="company" placeholder="enter the company..">
                                @if ($errors->has('company'))<span
                                        class="help-block error_font"><strong>{{ $errors->first('company') }}</strong></span>@endif
                            </fieldset>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 list">
                            <fieldset class="{{ $errors->has('name') ? ' has-error' : '' }}">
                                <input type="text" id="name" name="name" placeholder="enter the name..">
                                @if ($errors->has('name'))<span
                                        class="help-block error_font"><strong>{{ $errors->first('name') }}</strong></span>@endif
                            </fieldset>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 list">
                            <fieldset class="{{ $errors->has('email') ? ' has-error' : '' }}">
                                <input type="text" id="email" name="email" placeholder="enter the email..">
                                @if ($errors->has('email'))<span
                                        class="help-block error_font"><strong>{{ $errors->first('email') }}</strong></span>@endif
                            </fieldset>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 list">
                            <fieldset class="{{ $errors->has('phone') ? ' has-error' : '' }}">
                                <input type="text" id="phone" name="phone" placeholder="enter the phone..">
                                @if ($errors->has('phone'))<span
                                        class="help-block error_font"><strong>{{ $errors->first('phone') }}</strong></span>@endif
                            </fieldset>
                            <br>
                            <br>
                            <input type="text" id="date" name="date" readonly value="<?php echo date("d / m / y");?>">
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 list">
                            <textarea name="yourmsg" placeholder="Your message*"></textarea>
                            <div class="special_offers">
                                <input type="checkbox" name="checkbox" value="">
                                I would like to get news about courses and special offers</div>
                        </div>
                    </div>




                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null text-center ">
                       <button input type="submit" name="submit" class="btn form_submit">ENQUIRE NOW</button>
                    </div>
                </div>
            </form>
        </div>
    </div>

    <div class="container-fluid lovings">
        <div class="container">
            <div data-wow-delay=".1s" class="col-lg-12 col-md-12 col-sm-12 col-xs-12 wow animated fadeIn">
                <img src="{{URL::asset('image/sanpshot/10.jpg') }}" width="900" height="145">
            </div>
        </div>
    </div>
@endsection


