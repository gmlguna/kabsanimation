<html>
<head>
    <body>
<h4>
    <center style="color:red;">
        @if(session()->has('message'))
            <div class="alert alert-success">
                {{ session()->get('message') }}
            </div>
        @endif
    </center>
</h4>

<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 register_content">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <img class="thumbnail" src="image/attractions.jpg" width="200" height="250">
    </div>

</div>


<div class="row">
    <div class="medium-6 large-5 columns">
        <form method="POST" enctype="multipart/form-data">
            {{csrf_field()}}
            <input type="file" name="image_upload"/>
            <small class="error">{{$errors->first('image_upload')}}</small>
            <input type="submit" name="submit"/>

        </form>
    </div>

</div>
</body>
</head>
</html>