@extends('layout.main')
@section('title', 'Autocad | Animation Center in Krishnagiri')
@section('keywords', 'Animation Center in Krishnagiri')
@section('description', 'Animation Center in Krishnagiri')
@section('content')



    <form action="/employe" method="post">
        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

        <div class="container-fluid form_color">
            <div class="container">

                <div data-wow-delay=".1s" class="col-md-12 col-sm-12 col-xs-12 col-lg-12 padding_left_right_null">
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 ">
                    </div>

                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 staff_form">

                        <h3 style="background-color:#383d46; text-align: center; text-shadow: 1px 0;">Log In</h3>

                        <div class="sizeses"><strong>User Name:</strong><span class="star-rating">*</span></div>
                        <fieldset class="{{ $errors->has('username') ? ' has-error' : '' }}">
                            <input class="input_color" types="text" id="username" name="username"
                                   placeholder="enter the  username.....">
                            @if ($errors->has('username'))<span
                                    class="help-block error_font"><strong>{{ $errors->first('username') }}</strong></span>@endif
                            <h5>
                                <center style="color:red;">
                                    @if(session()->has('msg'))
                                        <div class="alert alert-success">
                                            {{ session()->get('msg') }}
                                        </div>
                                    @endif
                                </center>

                                <center style="color:black;">
                                    @if(session()->has('message'))
                                        <div class="alert alert-success">
                                            {{ session()->get('message') }}
                                        </div>
                                    @endif
                                </center>
                            </h5>
                        </fieldset>

                        <div class="sizeses"><strong>Password:</strong><span class="star-rating">*</span></div>
                        <fieldset class="{{ $errors->has('password') ? ' has-error' : '' }}">
                            <input class="input_color" type="password" id="password" name="password"
                                   placeholder="enter the  password.....">
                            @if ($errors->has('password'))<span
                                    class="help-block error_font"><strong>{{ $errors->first('password') }}</strong></span>@endif

                        </fieldset>


                        <div class="button_submit">
                            <input type="submit" value="Submit">&nbsp;
                            <input type="reset" value="Clear">&nbsp;
                        </div>


                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 ">

                    </div>
                </div>


            </div>
        </div>
    </form>
@endsection