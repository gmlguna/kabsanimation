<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content='@yield('keywords')'>
    <meta name="description" content='@yield('description')'>
    <title>Kabs Animation - @yield('title')</title>
    <link rel="stylesheet" href="{{URL::asset('css/bootstrap.css') }}">
    <link rel="stylesheet" href="{{URL::asset('css/style.css') }}">
    <link rel="stylesheet" href="{{URL::asset('css/content.css') }}">
    <link rel="stylesheet" href="{{URL::asset('css/animate.css') }}">
    <link rel="stylesheet" href="{{URL::asset('css/responsive.css') }}">
    <script src="{{URL::asset('js/jquery.js') }}"></script>
    <script src="{{URL::asset('js/bootstrap.js') }}"></script>
    <script src="{{URL::asset('js/wow.js') }}"></script>
    <script src="{{URL::asset('js/custom.js') }}"></script>
</head>
<body>
<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
<div class="container-fluid">
    <div class="container certification_border">
<div class="container-fluid indival">
    <div class="container">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12 ">
            </div>

            <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12 academy_class">
                <div class="certifi_kabs"><strong>KABS ANIMATION</strong></div>
                <div class="certifi_kabs"><p><strong>No 236c, Newpet, Near Daily Thanthi Office, Roundana
                            Krishnagiri.</strong></div>
                <div class="certifi_kabs"><p><strong> Krishnagiri - 635 601, Tamil Nadu, S.India</strong></p></div>
                <div class="certifi_kabs"><p><strong>Phone :822 045 6017 Email:Admin@kabsanimation.com </strong></p>
                </div>
            </div>
            <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 academy_class">
                <p><strong> <img src="{{URL::asset('image/ani.png') }}" width="135" height="100"></strong></p>
                <p><strong>Ready for<br>Every Good Work</strong></p>
            </div>
        </div>
    </div>
</div>


<div class="container-fluid">
    <div class="container">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <p><strong>COURSE COMPLETION CERTIFICATE</strong></p>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="container">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
            </div>
            <div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
                <div class="cert_para">This is to certify that <strong> <?php echo $users[0]->name; ?> </strong>
                    Register No. <strong> <?php echo $users[0]->regno; ?></strong> has join date of courese
                    <strong> <?php echo $users[0]->joindate; ?></strong>
                    student of this kabs animation class studied in has completed the course
                    <strong> <?php echo $users[0]->courses; ?></strong> at the course Start
                    Date<strong> <?php echo $users[0]->startdate; ?> </strong> and End date
                    <strong> <?php echo $users[0]->enddate; ?> </strong>at the during the
                   <strong> <?php echo $users[0]->duration; ?> </strong> course completed.  His conduct and character were good.
                </div>
            </div>
            <div class="col-lg-1 col-md-1 col-sm-1 col-xs-12">
            </div>
        </div>
    </div>
</div>



<div class="container-fluid issue">
    <div class="container">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="cert_parase"> <strong> Issue date:</strong> <strong> <?php echo $users[0]->startdate; ?> </strong></div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12">
                <div class="cert_para"> <strong> Signature</strong> </div>
            </div>
        </div>
    </div>
</div>
</div>
    </div>



</body>
</html>






