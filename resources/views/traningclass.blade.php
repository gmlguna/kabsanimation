@extends('layout.main')
@section('title', 'Autocad | Animation Center in Krishnagiri')
@section('keywords', 'Animation Center in Krishnagiri')
@section('description', 'Animation Center in Krishnagiri')
@section('content')
    <h2 class="student_subhead" style="background-color:lightslategrey;"> <div class="welcome"><strong class="welcomekbas"> Welcome to Kabs animation </strong>
            <div class="right_logout">
                <a class="logout" href="{{ url('/staff_registerdetails') }}"><strong>StaffDetails</strong></a>
                <a class="logout" href="{{ url('/studentregister_details') }}"><strong>StudentDetails</strong></a>
                <a class="logout" href="{{ url('/traningdetails') }}"><strong>TraningClass Details</strong></a>
                <a class="logout" href="{{ url('/empenquire') }}"><strong>EnquireDetails</strong></a>
                <a class="logout" href ="employe"><strong>Logout</strong></a>
            </div>
        </div>
    </h2>
    <h4>
        <center style="color:red;">
            @if(session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif
        </center>
    </h4>

    <form action="/traningclass" method="post">
        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

        <div class="container-fluid form_color">
            <div class="container">
                <form method="post" action="#">
                    <div data-wow-delay=".1s" class="col-md-12 col-sm-12 col-xs-12 col-lg-12 padding_left_right_null">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 ">
                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 ">
                            <h3 style="background-color:lightslategrey; text-align: center; text-shadow: 1px 0;">Traning Class Plan</h3>


                            <div class="sizes"><strong>Staff Name:</strong><span class="star-rating">*</span></div>
                            <fieldset class="{{ $errors->has('name') ? ' has-error' : '' }}">
                                <input type="text" id="name" name="name" placeholder="enter staff the Name..">
                                @if ($errors->has('name'))<span
                                        class="help-block error_font"><strong>{{ $errors->first('name') }}</strong></span>@endif

                            </fieldset>

                            <div class="sizes"><strong>Courses</strong><span class="star-rating">*</span>
                            </div>
                            <fieldset class="{{ $errors->has('courses') ? ' has-error' : '' }}">
                                <select id="courses" name="courses">
                                    <option value="">Select the Courses</option>
                                    <option value="Photoshop">Photoshop</option>
                                    <option value="InDesign">InDesign</option>
                                    <option value="Illustrator">Illustrator</option>
                                    <option value="AfterEffects">AfterEffects</option>
                                </select>
                                @if ($errors->has('courses'))<span
                                        class="help-block error_font"><strong>{{ $errors->first('courses') }}</strong></span>@endif
                            </fieldset>

                            <div class="sizes"><strong>Date:</strong><span class="star-rating">*</span>
                            </div>
                            <fieldset class="{{ $errors->has('date') ? ' has-error' : '' }}">
                                <input type="date" id="date" name="date">
                                @if ($errors->has('date'))<span
                                        class="help-block error_font"><strong>{{ $errors->first('date') }}</strong></span>@endif
                            </fieldset>

                            <div class="sizes"><strong>Class Shift</strong><span class="star-rating">*</span>
                            </div>
                            <fieldset class="{{ $errors->has('time') ? ' has-error' : '' }}">
                                <select id="time" name="time">
                                    <option value="">Select the Shift</option>
                                    <option value="Morning">Morning</option>
                                    <option value="Afternoon">Afternoon</option>
                                </select>
                                @if ($errors->has('time'))<span
                                        class="help-block error_font"><strong>{{ $errors->first('time') }}</strong></span>@endif
                            </fieldset>

                            <div class="button_submit">
                                <input type="submit" value="Submit">&nbsp;
                            </div>

                        </div>
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-12 ">

                        </div>
                    </div>
                </form>
            </div>
        </div>

@endsection