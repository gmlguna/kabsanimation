

@extends('layout.main')
@section('title', 'Autocad | Animation Center in Krishnagiri')
@section('keywords', 'Animation Center in Krishnagiri')
@section('description', 'Animation Center in Krishnagiri')
@section('content')


    <h2 class="student_subhead" style="background-color:lightslategrey;"> <div class="welcome"><strong class="welcomekbas"> Welcome to Kabs animation </strong>
            <div class="right_logout">
                <a class="logout" href="{{ url('/staff_registerdetails') }}"><strong>StaffDetails</strong></a>
                <a class="logout" href="{{ url('/studentregister_details') }}"><strong>StudentDetails</strong></a>
                <a class="logout" href="{{ url('/traningdetails') }}"><strong>TraningClass Details</strong></a>
                <a class="logout" href="{{ url('/empenquire') }}"><strong>EnquireDetails</strong></a>
                <a class="logout" href ="employe"><strong>Logout</strong></a>
            </div>
        </div>
    </h2>

    <h4>
        <center style="color:red;">
            @if(session()->has('message'))
                <div class="alert alert-success">
                    {{ session()->get('message') }}
                </div>
            @endif
        </center>
    </h4>



    <form action="{{ url('/traning/edit/'.$users[0]->traning) }}" method="post">
        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">

        <table  border ="8">

            <tr class="boldtable" bgcolor="#d3d3d3">
                <th>Name:</th>

                <td>
                    <input type='text' name='name'
                           value='<?php echo $users[0]->name; ?>'/>
                </td>
            </tr>
            <tr class="boldtable" bgcolor="#d3d3d3">
                <th>Courses</th>

                <td>
                    <input type='text' name='courses'
                           value='<?php echo $users[0]->courses; ?>'/>
                </td>
            </tr>

            <tr class="boldtable" bgcolor="#d3d3d3">
                <th>Date:</th>

                <td>
                    <input type='text' name='date'
                           value='<?php echo $users[0]->date; ?>'/>
                </td>
            </tr>
            <tr class="boldtable" bgcolor="#d3d3d3">
                <th>Time</th>

                <td>
                    <input type='text' name='time'
                           value='<?php echo $users[0]->time; ?>'/>
                </td>
            </tr>

            <tr bgcolor="#d3d3d3">
                <th></th>

                <td>
                    <input type='submit' value="Update "/>
                </td>
            </tr>




        </table>
    </form>



@endsection





