@extends('layout.main')
@section('title', 'Indesign | Animation Center in Krishnagiri')
@section('keywords', 'Animation Center in Krishnagiri')
@section('description', 'Animation Center in Krishnagiri')
@section('content')

    <div class="container-fluid indesign_home common_bg_style">
        <h4>
            <center style="color:red;">
                @if(session()->has('message'))
                    <div class="alert alert-success">
                        {{ session()->get('message') }}
                    </div>
                @endif
            </center>
        </h4>
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-5 col-md-5 col-sm-5 col-xs-12 adobe_after">
                    <strong>Adobe InDesign <br>Training Courses</strong>
                    <h5 class="strong"><strong>Adobe InDesign is the Industry Standard Application for publishing
                            projects:</strong>
                        <ul class="h5 strong">
                            <li>posters, leaflets and brochures</li>
                            <li>printed books, reports and magazines</li>
                            <li>e-books.</li>
                        </ul>
                    </h5>
                    <h5 class="strong"><strong>Used in graphic design and page layouts for print and digital, it’s a
                            powerful tool and easy-to-use because of its integration with other Adobe applications. 18-month
                            Free class retake included.</strong>
                    </h5>
                    <img alt="adobe" title="adobe" src="{{URL::asset('image/after/adobe.jpg') }}" width="150" height="50">
                </div>
                <div class="col-lg-7 col-md-7 col-sm-7 col-xs-12">
                    <div class="after_video">
                        <iframe class="iframe"
                                src="https://www.youtube.com/embed/MYWgcAqqvL8?rel=0&amp;showinfo=0"></iframe>
                    </div>
                </div>
            </div>
        </div>


        <div class="container-fluid ss_sanpshot">
            <div class="container">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 training_courses button">
                        <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null" href="{{ url('/') }}">
                            <button type="button" class="moredetails" class="moredetails-arrow-down">MORE DETAILS</button>
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 training_courses">
                        <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null" href="{{ url('/') }}">
                            <button type="button" class="charwith">CHAT WITH US</button>
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 training_courses">
                        <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null" href="{{ url('/') }}">
                            <button type="button" class="phonenumber">8220456017</button>
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 training_courses">
                        <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null" href="{{ url('/enquri') }}">
                            <button type="button" class="enquire">ENQUIRE NOW</button>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 choose_your">
                    <div class="descript"><strong>Class Snapshots:</strong></div>
                    <img class="adobe" src="{{URL::asset('image/sanpshot/1.jpg') }}"><br>
                    <img class="adobe" src="{{URL::asset('image/sanpshot/2.jpg') }}"><br>
                    <img class="adobe" src="{{URL::asset('image/sanpshot/3.jpg') }}"><br>
                    <img class="adobe" src="{{URL::asset('image/sanpshot/4.jpg') }}"><br>
                    <img class="adobe" src="{{URL::asset('image/sanpshot/5.jpg') }}"><br>
                    <img class="adobe" src="{{URL::asset('image/sanpshot/6.jpg') }}"><br>
                    <img class="adobe" src="{{URL::asset('image/sanpshot/7.jpg') }}"><br>
                    <img class="adobe" src="{{URL::asset('image/sanpshot/3.jpg') }}"><br>
                </div>


                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 course_information">
                    <h2><strong> Course Information:</strong></h2>
                    <div class="description"><strong>Adobe InDesign Course Description</strong></div>
                    <div class="course_information_paragraph">Our Adobe-certified training courses cover the use of Adobe
                        InDesign for desktop and electronic publishing, including operations typical in producing magazines,
                        newspapers, catalogues, booklets, newsletters, brochures, reports, fliers and other similar
                        publications. At kabs animation Class, we build your Adobe InDesign expertise from the ground up in all the
                        essential features of the software through practical sessions, real-world illustrations and cover
                        many indispensable design techniques. Our Adobe-accredited and industry-experienced instructors will
                        make sure you learn the essential skills you need in designing for print to producing rich
                        interactive documents for tablets, smartphones and computers.
                    </div>
                    <div class="description"><strong>What will you learn?</strong></div>
                    <div class="course_information_paragraph">
                        <ul>
                            <li>These Adobe InDesign courses feature in-class preparation, supportive learning environment,
                                industry-certified trainers and up-to-date syllabus.
                            </li>

                            <li>kabs animation Class provides you state-of-the-art Mac and PC workstations to facilitate advanced
                                learning.
                            </li>

                            <li>kabs animation Class offers you Adobe InDesign courses in a number of cities, namely: London,
                                Glasgow, Manchester, Cardiff, Newcastle and Birmingham. .
                            </li>

                            <li>If after taking the InDesign course with kabs animation class you don’t get a chance to use the
                                technology or forget something then you are more than welcome to come back and retake the
                                class FREE OF CHARGE. That’s the kabs animation Class GUARANTEE of learning.
                            </li>

                            <li>All our classes at kabs animation Class begin at 9.30am and running till 4:30pm. Enroll now in one
                                of the best InDesign, Adobe-authorized training courses!
                            </li>
                        </ul>
                    </div>

                    <div class="description"><strong>At the end of these training courses you will:</strong></div>
                    <div class="course_information_paragraph"><img src="{{URL::asset('image/cb.jpg') }}" width="20" height="20"> be able to
                        create impressive vector images with the help of Adobe Illustrator.
                    </div>

                    <div class="course_information_paragraph"><img src="{{URL::asset('image/cb.jpg') }}" width="20" height="20"> learn how to
                        work on graphic projects based on layers, color functions, Pen tool, transformation of existing
                        shapes and adding color to drawn objects.
                    </div>

                    <div class="course_information_paragraph"><img src="{{URL::asset('image/cb.jpg') }}" width="20" height="20"> also learn to
                        use the powerful features like multiple art boards and transparent gradients, and use layers to
                        organize your artwork together with exploring the options for exporting Adobe Illustrator files.
                    </div>

                    <div class="course_information_paragraph"><img src="{{URL::asset('image/cb.jpg') }}" width="20" height="20"> use the
                        special effects like:
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 course_information_paragraph ">
                        <ul>
                            <li>masking techniques,
                            </li>

                            <li>blending modes and how to use the blend tool,
                            </li>

                            <li>create a blend within text,
                            </li>
                            <li>mastering compound paths,
                            </li>

                            <li>3D simulation through shading,
                            </li>
                        </ul>
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 course_information_paragraph">
                        <ul>
                            <li>isometric representation of objects,
                            </li>

                            <li>blending modes and how to use the blend tool,
                            </li>

                            <li>using pathfinder operations,
                            </li>
                            <li>creating text effects
                            </li>

                            <li>and much more!
                            </li>
                        </ul>
                    </div>


                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="description"><strong>Blended Learning</strong></div>

                        <div class="course_information_paragraph">It’s the best opportunity to get the most out of your
                            learning
                            experience while blending technology with classroom instructions. We supply:
                            <ul class="h5 strong">
                                <li>training videos,</li>
                                <li>notes and/or</li>
                                <li>reference texts.</li>
                            </ul>
                        </div>
                    </div>

                    <h2 class="learn"><strong> How You Want To Learn</strong></h2>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 course_information">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <img class="right" src="{{URL::asset('image/cb.jpg') }}" width="20" height="20">
                            <div class="course_information_paragraphs"><strong>Individual</strong></div>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 course_information">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <img class="right" src="{{URL::asset('image/cb.jpg') }}" width="20" height="20">
                            <div class="course_information_paragraphs"><strong>Customis<br>ed</strong></div>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 course_information">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <img class="right" class="right" src="image/cb.jpg" width="20" height="20">
                            <div class="course_information_paragraphs"><strong>Classroom</strong></div>
                        </div>
                    </div>

                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12 course_information">
                        <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
                            <img src="{{URL::asset('image/cb.jpg') }}" width="20" height="20">
                            <div class="right" class="course_information_paragraphs"><strong> Live-Online </strong></div>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <h1><strong>Still Not Convinced?</strong></h1>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="still"><strong>18-Month Free<br>Class Retake</strong>
                        </div>
                        <h>If you have any gaps in your knowledge or want to refresh your skills, you are more than
                            welcome to come back  live online class free of charge up to 18 months after
                            you have taken the class.
                        </h>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="still"><strong>Money-Back <br>
                                Guarantee</strong>
                        </div>
                        <h>If you don’t absolutely LOVE your class, we’ll give you a full refund! Let us know on the
                            FIRST day of your training if something isn’t quite right and give us a chance to fix it or
                            give you your money back.
                        </h>
                    </div>


                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="still"><strong>Lower Price<br>Guarantee</strong>
                        </div>
                        <h>We think our prices are pretty fair but we won’t be beaten on our fee. We’ll match and
                            discount by 10% any like-for-like Training course price.
                        </h>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="still"><strong>Experienced<br>Instructors</strong>
                        </div>
                        <h>Equipped with years of industry experience our instructors will assure a successful leap in
                            your knowledge, improvement and preparation.
                        </h>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="two"><strong> “The investment in knowledge pays the best interests.”</strong></div>
                            <br>
                            <p class="frank">~ Benjamin Franklin</p>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="container-fluid ss_sanpshot">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 training_courses button">
                    <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null" href="{{ url('/') }}">
                        <button type="button" class="moredetails" class="moredetails-arrow-down">MORE DETAILS</button>
                    </a>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 training_courses">
                    <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null" href="{{ url('/') }}">
                        <button type="button" class="charwith">CHAT WITH US</button>
                    </a>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 training_courses">
                    <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null" href="{{ url('/') }}">
                        <button type="button" class="phonenumber">8220456017</button>
                    </a>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 training_courses">
                    <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null" href="{{ url('/enquri') }}">
                        <button type="button" class="enquire">ENQUIRENOW</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid course_time">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 course_times">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null">
                        <h3 class="course-time"><span
                                    style="color: #ffffff;">Course<br> Times:</span>
                        </h3>
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 course_times">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null">
                        <img src="image/course1.png" width="70" height="70">
                        <br>
                        <br>
                        <p style="text-align: center;">
                            <span style="color: #00969d; margin-top: 5px;">9:00 – 11:00</span><br>
                            <span style="color: #ffffff;">Course</span>
                        </p>
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 course_times">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null">
                        <img src="image/course2.jpg" width="70" height="70">
                        <br>
                        <br>
                        <p style="text-align: center;">
                            <span style="color: #00969d; margin-top: 5px;">11:00 – 11:15</span><br>
                            <span style="color: #ffffff;">Break</span>
                        </p>
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 course_times">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null">
                        <img src="image/course3.png" width="70" height="70">
                        <br>
                        <br>
                        <p style="text-align: center;">
                            <span style="color: #00969d; margin-top: 5px;">11:15 – 13:00</span><br>
                            <span style="color: #ffffff;">Course</span>
                        </p>
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 course_times">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null">
                        <img src="image/course4.png" width="70" height="70">
                        <br>
                        <br>
                        <p style="text-align: center;">
                            <span style="color: #00969d; margin-top: 5px;">13:00 – 14:00</span><br>
                            <span style="color: #ffffff;">Break</span>
                        </p>
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 course_times">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null">
                        <img src="image/course5.png" width="70" height="70">
                        <br>
                        <br>
                        <p style="text-align: center;">
                            <span style="color: #00969d; margin-top: 5px;">14:00 – 16:30</span><br>
                            <span style="color: #ffffff;">Course</span>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <h2 class="course_leaves"><strong>Course Levels</strong></h2>
                    <div class="single_course"> SINGLE COURSES</div>

                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <img src="{{URL::asset('image/single1.jpg') }}" width="45" height="45">
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                            <div class="yellow"><strong>Yellow Belt:<br>101</strong></div>
                            <div class="package">Beginners</div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <img src="{{URL::asset('image/single2.jpg') }}" width="45" height="45">
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                            <div class="yellow"><strong>Green Belt:<br>201</strong></div>
                            <div class="package">Intermediate</div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <img src="{{URL::asset('image/single3.jpg') }}" width="45" height="45">
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                            <div class="yellow"><strong>Black Belt:<br>301</strong></div>
                            <div class="package">Advanced</div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <img src="{{URL::asset('image/single4.jpg') }}" width="45" height="45">
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                            <div class="yellow"><strong>Certified: 401</strong></div>
                            <div class="package">Expert</div>
                        </div>
                    </div>

                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="single_course">PACKAGED COURSES</div>
                        </div>
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <img src="{{URL::asset('image/single5.jpg') }}" width="45" height="45">
                        </div>
                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                            <div class="yellow"><strong>Zero to Hero</strong></div>
                            <div class="packagese">Complete<br>
                                courses 101,<br>
                                201 and 301 in a<br>
                                combined and<br>
                                discounted<br>
                                package.
                            </div>
                        </div>
                    </div>

                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
                            <img src="{{URL::asset('image/single6.jpg') }}" width="45" height="45">
                        </div>

                        <div class="col-lg-9 col-md-9 col-sm-9 col-xs-12">
                            <div class="yellow"><strong>Ultimate<br>Mastery</strong></div>
                            <div class="packagese">Complete<br>
                                courses 101,<br>
                                201, 301 and<br>
                                401 in a<br>
                                combined and<br>
                                discounted<br>
                                package.
                            </div>
                        </div>
                    </div>

                </div>


                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <div class="description">Free Whitepaper – Infographics</div>
                    <div class="course_information_paragraph">A study by the Wharton School of Business found that with a
                        presentation, 50% of the audience were persuaded by verbal content only. Once visuals were added to
                        the presentation, 67% became persuaded.
                    </div>
                    <div class="course_information_paragraph">An infographic by Periscopic attempted to quantify the number
                        of years of life ‘stolen’ by gun crime in the US. Periscopic felt ‘compelled’ to share the
                        infographic with the world, and the world took notice.
                    </div>
                    <div class="course_information_paragraph">Humans are visual animals. 70% of our sensory receptors are in
                        our eyes. We can get a sense of a visual scene in less than 1/10th of a second. That’s why we find
                        infographics engaging, shareable, inspirational.
                    </div>
                    <div class="course_information_paragraph">In the whitepaper, you’ll learn what infographics are, why
                        they’re so important, and how to make them yourself. Download today and add infographics to your
                        communications armoury. <a href="{{ url('/') }}">
                            <div class="more_info"><strong> Download your free copy here. link test</strong></div>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 course">
                <h2>That’s us. Now it’s up to you. Enquire now!</h2>
            </div>
        </div>
    </div>
    <div class="container-fluid form_image common_bg_style">
        <div class="container">
            <form action="/home" method="post">
                <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
                <div data-wow-delay=".1s" class ="col-md-12 col-sm-12 col-xs-12 col-lg-12 padding_left_right_null wow animated zoomIn">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <fieldset class="{{ $errors->has('location') ? ' has-error' : '' }}">
                                <select id="location" name="location">
                                    <option value="location">Choose Location</option>
                                    <option value="krishnagiri">Krishnagiri</option>
                                    <option value="vellore">Vellore</option>
                                    <option value="tirupattur">Tirupattur</option>
                                    <option value="hosur">hosur</option>
                                    <option value="chennai">chennai</option>
                                </select>
                                @if ($errors->has('location'))<span
                                        class="help-block error_font"><strong>{{ $errors->first('location') }}</strong></span>@endif
                            </fieldset>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 list">
                            <fieldset class="{{ $errors->has('company') ? ' has-error' : '' }}">
                                <input type="text" id="company" name="company" placeholder="enter the company..">
                                @if ($errors->has('company'))<span
                                        class="help-block error_font"><strong>{{ $errors->first('company') }}</strong></span>@endif
                            </fieldset>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 list">
                            <fieldset class="{{ $errors->has('name') ? ' has-error' : '' }}">
                                <input type="text" id="name" name="name" placeholder="enter the name..">
                                @if ($errors->has('name'))<span
                                        class="help-block error_font"><strong>{{ $errors->first('name') }}</strong></span>@endif
                            </fieldset>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 list">
                            <fieldset class="{{ $errors->has('email') ? ' has-error' : '' }}">
                                <input type="text" id="email" name="email" placeholder="enter the email..">
                                @if ($errors->has('email'))<span
                                        class="help-block error_font"><strong>{{ $errors->first('email') }}</strong></span>@endif
                            </fieldset>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 list">
                            <fieldset class="{{ $errors->has('phone') ? ' has-error' : '' }}">
                                <input type="text" id="phone" name="phone" placeholder="enter the phone..">
                                @if ($errors->has('phone'))<span
                                        class="help-block error_font"><strong>{{ $errors->first('phone') }}</strong></span>@endif
                            </fieldset>
                            <br>
                            <br>
                            <input type="text" id="date" name="date" readonly value="<?php echo date("d / m / y");?>">
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12 list">
                            <textarea name="yourmsg" placeholder="Your message*"></textarea>
                            <div class="special_offers">
                                <input type="checkbox" name="checkbox" value="">
                                I would like to get news about courses and special offers</div>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null text-center ">
                        <button input type="submit" name="submit" class="btn form_submit">ENQUIRE NOW</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="container-fluid ss_sanpshot">
        <div class="container">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 training_courses button">
                    <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null" href="{{ url('/') }}">
                        <button type="button" class="moredetails" class="moredetails-arrow-down">MORE DETAILS</button>
                    </a>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 training_courses">
                    <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null" href="{{ url('/') }}">
                        <button type="button" class="charwith">CHAT WITH US</button>
                    </a>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 training_courses">
                    <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null" href="{{ url('/') }}">
                        <button type="button" class="phonenumber">8220456017</button>
                    </a>
                </div>
                <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12 training_courses">
                    <a class="col-lg-12 col-md-12 col-sm-12 col-xs-12 padding_left_right_null" href="{{ url('/enquri') }}">
                        <button type="button" class="enquire">ENQUIRENOW</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
@endsection