-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 23, 2018 at 03:52 PM
-- Server version: 5.7.23-0ubuntu0.16.04.1
-- PHP Version: 7.0.30-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kabsanimation`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`username`, `password`) VALUES
('admin', 'admin@123');

-- --------------------------------------------------------

--
-- Table structure for table `employe`
--

CREATE TABLE `employe` (
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employe`
--

INSERT INTO `employe` (`username`, `password`) VALUES
('employe', 'employe@123');

-- --------------------------------------------------------

--
-- Table structure for table `home`
--

CREATE TABLE `home` (
  `id` int(225) NOT NULL,
  `location` varchar(100) NOT NULL,
  `company` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `yourmsg` varchar(100) NOT NULL,
  `date` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `home`
--

INSERT INTO `home` (`id`, `location`, `company`, `name`, `email`, `phone`, `yourmsg`, `date`) VALUES
(40, 'vellore', 'anadth', 'gunas', 'guna1081994@gmail.com', '9876543210', 'good', '11 / 07 / 18'),
(41, 'tirupattur', 'gml tech', 'madhan', 'guna@gmail.com', '1234567890', 'good', '11 / 07 / 18'),
(42, 'hosur', 'gml tech', 'gunas', 'guna@gmail.com', '36236563656', 'good tech class', '11 / 07 / 18'),
(43, 'chennai', 'gml tech', 'madhan', 'guna@gmail.com', '9876543210', 'tirupatturi', '11 / 07 / 18'),
(44, 'tirupattur', 'kabsanimation', 'sakthi', 'me@gmail.com', '9876543210', 'good excelent class', '11 / 07 / 18'),
(45, 'vellore', 'gml tech', 'thirupathi', 'thiru@gmail.com', '9876543210', 'good communition', '11 / 07 / 18'),
(46, 'vellore', 'gml tech', 'madhi', 'guna1081994@gmail.com', '36236563656', 'welcome', '13 / 09 / 18');

-- --------------------------------------------------------

--
-- Table structure for table `staff`
--

CREATE TABLE `staff` (
  `staff_id` int(255) NOT NULL,
  `name` varchar(100) NOT NULL,
  `dob` varchar(100) NOT NULL,
  `mobileno` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `gender` varchar(100) NOT NULL,
  `joindate` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL,
  `image_upload` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `staff`
--

INSERT INTO `staff` (`staff_id`, `name`, `dob`, `mobileno`, `email`, `gender`, `joindate`, `address`, `image_upload`) VALUES
(12, 'madhan', '2018-06-25', '9876543210', 'guna1081994@gmail.com', 'female', '2018-07-31', 'krishnagiri', 'nsch2.jpg'),
(13, 'guna', '2018-07-03', '987654333', 'gunaseakran@gmail.com', 'male', '2018-07-23', 'tpt', 'nsch1.jpg'),
(14, 'sakthi', '2018-07-31', '3412345435', 'me@gmail.com', 'male', '2018-07-20', 'vellore', 'shiv2.jpg'),
(15, 'deapan', '2018-07-19', '9876543210', 'guna1081994@gmail.com', 'female', '2018-07-30', 'vellore', 'ishasharma.jpeg'),
(16, 'madhi', '2018-06-24', '9876543210', 'gunaseakran@gmail.com', 'male', '2018-07-31', 'chennai', 'hqdefault.jpg'),
(17, 'karthi', '2018-07-04', '9087654321', 'guna1081994@gmail.com', 'male', '2018-08-03', 'tpt', 'im1.jpg'),
(18, 'madhi', '2018-07-03', '3412345435465546', 'me@gmail.com', 'male', '2018-06-30', 'vellore', 'nsch2.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `regno` varchar(100) NOT NULL,
  `name` varchar(100) NOT NULL,
  `dob` date NOT NULL,
  `mobileno` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `gender` varchar(100) NOT NULL,
  `startdate` date NOT NULL,
  `enddate` date NOT NULL,
  `duration` varchar(100) NOT NULL,
  `joindate` date NOT NULL,
  `courses` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL,
  `image_upload` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`regno`, `name`, `dob`, `mobileno`, `email`, `gender`, `startdate`, `enddate`, `duration`, `joindate`, `courses`, `address`, `image_upload`) VALUES
('kb151018', 'guna', '2018-09-14', '1234456676', 'guna@gmail.com', 'male', '2018-09-14', '2018-09-28', '3 Month', '2018-09-14', 'Photoshop', 'chennai', 'dot.png'),
('kb151022', 'gunasekaran', '2018-07-30', '341234543', 'me@gmail.com', 'male', '2018-07-26', '2018-07-23', '3 Month', '2018-07-19', 'Photoshop', 'welcome to all', 'thej.jpg'),
('kb151027', 'madhan', '2018-07-30', '9876543210', 'guna1081994@gmail.com', 'male', '2018-07-30', '2018-07-02', '6 Month', '2018-07-25', 'Photoshop', 'kurumberi', 'srie.jpg'),
('kb151028', 'gunas', '2018-07-30', '341234543', 'guna1081994@gmail.com', 'female', '2018-07-16', '2018-07-26', '3 Month', '2018-07-28', 'Photoshop', 'krishnagiri', 'slider3.jpg'),
('kb151030', 'madhi', '2018-07-30', '9876543210', 'me@gmail.com', 'female', '2018-07-23', '2018-07-30', '3 Month', '2018-07-22', 'Photoshop', 'vellore', 'popup.png'),
('kb151035', 'kannan', '2018-07-31', '9087654321', 'me@gmail.com', 'male', '2018-07-31', '2018-07-22', '3 Month', '2018-07-28', 'Photoshop', 'kurumberi', 'award2a.JPG');

-- --------------------------------------------------------

--
-- Table structure for table `traning`
--

CREATE TABLE `traning` (
  `traning` int(225) NOT NULL,
  `name` varchar(100) NOT NULL,
  `courses` varchar(100) NOT NULL,
  `date` varchar(100) NOT NULL,
  `time` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `traning`
--

INSERT INTO `traning` (`traning`, `name`, `courses`, `date`, `time`) VALUES
(65, 'vijay', 'Photoshops', '2018-07-22', 'Morning'),
(68, 'gunasekaran', 'Photoshop', '2018-07-3', 'Morning'),
(72, 'madhi', 'Photoshop', '2018-07-23', 'Afternoon'),
(73, 'gunas', 'Illustrator', '2018-07-23', 'Morning');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `home`
--
ALTER TABLE `home`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `staff`
--
ALTER TABLE `staff`
  ADD PRIMARY KEY (`staff_id`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`regno`);

--
-- Indexes for table `traning`
--
ALTER TABLE `traning`
  ADD PRIMARY KEY (`traning`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `home`
--
ALTER TABLE `home`
  MODIFY `id` int(225) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `staff`
--
ALTER TABLE `staff`
  MODIFY `staff_id` int(255) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `traning`
--
ALTER TABLE `traning`
  MODIFY `traning` int(225) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=74;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
