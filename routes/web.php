<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});



Route::get('/upimage', function () {
    return view('upimage');
});

//Route::post('upimage','ImageController@upload')->name('upload');

Route::get('/drop', function () {
    return view('drop');
});


Route::get('/home', function () {
    return view('home');
});


Route::get('/adobe', function () {
    return view('adobe');
});

Route::get('/after_effects', function () {
    return view('after_effects');
});

Route::get('/animate', function () {
    return view('animate');
});

Route::get('/captivate', function () {
    return view('captivate');
});

Route::get('/creativecloud', function () {
    return view('creativecloud');
});

Route::get('/expresiencedesign', function () {
    return view('expresiencedesign');
});

Route::get('/illustrator', function () {
    return view('illustrator');
});

Route::get('/indesign', function () {
    return view('indesign');
});

Route::get('/lightroom', function () {
    return view('lightroom');
});

Route::get('/muse', function () {
    return view('muse');
});

Route::get('/photoshop', function () {
    return view('photoshop');
});

Route::get('/premierepro', function () {
    return view('premierepro');
});

Route::get('/dsmax', function () {
    return view('dsmax');
});

Route::get('/autocad', function () {
    return view('autocad');
});

Route::get('/fusion', function () {
    return view('fusion');
});

Route::get('/inventor', function () {
    return view('inventor');
});

Route::get('/bulindinginformation', function () {
    return view('bulindinginformation');
});

Route::get('/maya', function () {
    return view('maya');
});

Route::get('/navisworks', function () {
    return view('navisworks');
});

Route::get('/revit', function () {
    return view('revit');
});

Route::get('/cinema', function () {
    return view('cinema');
});

Route::get('/hololens', function () {
    return view('hololens');
});

Route::get('/unity', function () {
    return view('unity');
});

Route::get('/unreal', function () {
    return view('unreal');
});

Route::get('/sketchup', function () {
    return view('sketchup');
});

Route::get('/zbrush', function () {
    return view('zbrush');
});

Route::get('/digitalart', function () {
    return view('digitalart');
});

Route::get('/designtequ', function () {
    return view('designtequ');
});

Route::get('/uxdesign', function () {
    return view('uxdesign');
});

Route::get('/davinci', function () {
    return view('davinci');
});

Route::get('/nuke', function () {
    return view('nuke');
});

Route::get('/autodesk', function () {
    return view('autodesk');
});

Route::get('/game', function () {
    return view('game');
});
Route::get('/quicksearch', function () {
    return view('quicksearch');
});

Route::get('/certification', function () {
    return view('certification');
});
Route::get('/learningpaths', function () {
    return view('learningpaths');
});

Route::get('/bespoke', function () {
    return view('bespoke');
});

Route::get('/enquri', function () {
    return view('enquri');
});



Route::get('upload','ContentsController@upload')->name('upload');
Route::post('upload','ContentsController@upload')->name('upload');

Route::get('home','HomeController@insertform');
Route::post('home','HomeController@insert');
Route::post('uploadimage','ContentsController@uploadimage');


Route::get('student_register','RegController@insertform');
Route::post('student_register','RegController@insert');

Route::get('staff_register','StaffController@insertform');
Route::post('staff_register','StaffController@insert');

Route::get('traningclass','TraningController@insertform');
Route::post('traningclass','TraningController@insert');

Route::get('userlogin','UserloginController@index');
Route::post('userlogin','UserloginController@insert');

Route::get('staff_login','StaffloginController@index');
Route::post('staff_login','StaffloginController@insert');

Route::get('admin','AdminController@index');
Route::post('admin','AdminController@insert');
//Route::post('home','LoController@check1');

Route::get('employe','EmployeController@index');
Route::post('employe','EmployeController@insert');

Route::get('student_delete','Student_deleteController@index');
Route::get('delete/{regno}','Student_deleteController@destroy');

Route::get('student_edit','StudentupdateController@index');
Route::get('edit/{regno}','StudentupdateController@show');
Route::post('student_edit/{regno}','StudentupdateController@edit');



Route::get('/ex', function () {
    return view('ex');
});

Route::get('/cer', function () {
    return view('cer');
});

Route::get('/employe_details', function () {
    return view('employe_details');
});

Route::get('/admin_details', function () {
    return view('admin_details');
});

Route::get('views/staff_delete','Staff_deleteController@index');
Route::get('views/delete/{staff_id}','Staff_deleteController@destroy');

Route::get('views/staff','StaffupdateController@index');
Route::get('views/edit/{staff_id}','StaffupdateController@show');
Route::post('views/edit/{staff_id}','StaffupdateController@edit');

Route::get('/userlogin', function () {
    return view('userlogin');
});


Route::get('/register_details', function () {
    return view('register_details');
});

Route::get('check/{user_id}','PdfController@index');

Route::get('traning/traning_delete','Traning_deleteController@index');
Route::get('traning/delete/{name}','Traning_deleteController@destroy');

Route::get('traning/traning_edit','TraningupdateController@index');
Route::get('traning/edit/{traning}','TraningupdateController@show');
Route::post('traning/edit/{traning}','TraningupdateController@edit');


Route::get('studentregister_details','Register_detailsController@index');

Route::get('staff_registerdetails','Staff_registerdetailsController@index');

Route::get('traningdetails','TraningdetailsController@index');

Route::get('pdfview',array('as'=>'pdfview','uses'=>'ItemController@pdfview'));

Route::get('empenquire','EmpEnquireController@index');

Route::get('enquire/enquire_delete','Enquire_deleteController@index');
Route::get('enquire/delete/{id}','Enquire_deleteController@destroy');

Route::get('admin_registerdetails','Admin_staffController@index');
Route::get('admin_studentdetails','Admin_studentController@index');
Route::get('admin_traningdetails','Admin_traningController@index');
Route::get('enquridetails','Admin_enquriController@index');

Route::get('cer','InvoiceController@index');


Route::get('/form',function(){
    return view('form');
});


Route::get('/phpfirebase_sdk','FirebaseController@index');
