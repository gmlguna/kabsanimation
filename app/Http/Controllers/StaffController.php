<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
//use App\Http\Controllers\Validator;
use Validator;

class StaffController extends Controller
{
    public function insertform()
    {
        return view('staff_register');
    }

    /**
     * @param Request $request
     */

    protected function groupPageSettingsValidator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required',
            'dob' => 'required|after:yesterday',
            'mobileno' => 'required',
            'email' => 'required|unique:staff|email',
            'gender' => 'required',
            'joindate' => 'required',
            'address' => 'required',

        ]);
    }


    public function insert(Request $request)
    {
//

//        $username_exist = DB::table('staff')->where('username','=',$request->username)->first();
//
//        if($username_exist){
//            return redirect()->back()->with('msg', 'username already exists');
//
//        }
        $request->validate([
            'name' => 'required',
            'dob' => 'required',
            'mobileno' => 'required',
            'email' => 'required',
            'gender' => 'required',
            'joindate' => 'required',
            'address' => 'required',
        ]);


        $image_upload= '';
        if($request->hasFile('image_upload')){

            $filename=$request->image_upload->getClientOriginalName();
            $request->image_upload->storeAs('public',$filename);
            $image_upload=$filename;

        }

        $name = $request->name;
        $dob = $request->dob;
        $mobileno = $request->mobileno;
        $email = $request->email;
        $gender = $request->gender;
        $joindate = $request->joindate;
        $address = $request->address;

//        var_dump($request->all());die;
        DB::insert('insert into staff (name, dob, mobileno, email, gender, joindate, address, image_upload) values(?,?,?,?,?,?,?,?)', [ $name, $dob, $mobileno, $email, $gender, $joindate, $address, $image_upload]);

//        echo "Record inserted successfully.";
        return redirect()->back()->with ('message',' Welcome to Kabs animation Register success ');
    }
}