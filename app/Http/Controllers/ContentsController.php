<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
//use DB;
//use App\Http\Requests;
use App\Http\Controllers\Controller;
//use App\Http\Controllers\Validator;
//use Validator;

class ContentsController extends Controller
{

    public  function upload(Request $request)
    {
        $data=[];
        if ($request->isMethod('post'))
        {
            $this->validate(
                $request,
                [
                    'image_upload' => 'mimes:jpeg,bmp,png'
                ]
            );

            Input::file('image_upload')->move('image', 'attractions.jpg');
            return redirect()->back()->with ('message',' Welcome to Kabs animation Register success ');
        }
        return view('/upload', $data);

    }

}