<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
//use App\Http\Controllers\Validator;
use Validator;

class TraningController extends Controller
{
    public function insertform()
    {
        return view('traningclass');
    }

    /**
     * @param Request $request
     */

    protected function groupPageSettingsValidator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required',
            'courses' => 'required',
            'date' => 'required',
            'time' => 'required',

        ]);
    }


    public function insert(Request $request)
    {

        $request->validate([
            'name' => 'required',
            'courses' => 'required',
            'date' => 'required',
            'time' => 'required',
        ]);


        $name = $request->name;
        $courses = $request->courses;
        $date = $request->date;
        $time = $request->time;

//        var_dump($request->all());die;
        DB::insert('insert into traning (name, courses, date, time) values(?,?,?,?)', [$name,  $courses, $date,  $time]);

//          return view('traningclass');
//        echo "Record inserted successfully.";
        return redirect()->back()->with ('message','Plan success ');

//
    }
}