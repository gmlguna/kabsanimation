<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
//use App\Http\Controllers\Validator;
use Validator;
use File;

class RegController extends Controller
{
    public function insertform()
    {
        return view('student_register');
    }

    /**
     * @param Request $request
     */

    protected function groupPageSettingsValidator(array $data)
    {
        return Validator::make($data, [
            'regno' => 'required|unique :student',
            'name' => 'required',
            'dob' => 'required|after:yesterday',
            'mobileno' => 'required',
            'email' => 'required|unique:student|email',
            'gender' => 'required',
            'startdate' => 'required',
            'enddate' => 'required|after:startdate',
            'duration' => 'required',
            'joindate' => 'required',
            'courses' => 'required',
            'address' => 'required',


        ]);
    }


    public function insert(Request $request)
    {

        $username_exist = DB::table('student')->where('regno','=',$request->regno)->first();

        if($username_exist){
            return redirect()->back()->with('msg', 'Registe number already exists');

        }
        $request->validate([
            'regno' => 'required',
            'name' => 'required',
            'dob' => 'required',
            'mobileno' => 'required',
            'email' => 'required',
            'gender' => 'required',
            'startdate' => 'required',
            'enddate' => 'required',
            'duration' => 'required',
            'joindate' => 'required',
            'courses' => 'required',
            'address' => 'required',

        ]);
        $image_upload= '';
        if($request->hasFile('image_upload')){

            $filename=$request->image_upload->getClientOriginalName();
            $request->image_upload->storeAs('public',$filename);
            $image_upload=$filename;

        }

        $regno = $request->regno;
        $name = $request->name;
        $dob = $request->dob;
        $mobileno = $request->mobileno;
        $email = $request->email;
        $gender = $request->gender;
        $startdate = $request->startdate;
        $enddate = $request->enddate;
        $duration = $request->duration;
        $joindate = $request->joindate;
        $courses = $request->courses;
        $address = $request->address;



//        var_dump($request->all());die;
        DB::insert('insert into student (regno, name, dob, mobileno, email, gender, startdate, enddate, duration, joindate, courses, address, image_upload) values(?,?,?,?,?,?,?,?,?,?,?,?,?)', [$regno, $name, $dob, $mobileno, $email, $gender, $startdate, $enddate, $duration, $joindate, $courses, $address, $image_upload]);

//        echo "Record inserted successfully.";
        return redirect()->back()->with ('message',' Welcome to Kabs animation Register success ');
    }
}