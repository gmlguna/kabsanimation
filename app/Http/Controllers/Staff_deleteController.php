<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class Staff_deleteController extends Controller {
    public function index(){
        $users = DB::select('select * from staff');
        return view('views/staff_delete',['users'=>$users]);
    }
    public function destroy($staff_id) {
        DB::delete('delete from staff where  staff_id= ?',[$staff_id]);
        return redirect()->back()->with ('message',' Staff Deleted ');
    }
}