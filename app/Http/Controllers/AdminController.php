<?php

namespace App\Http\Controllers;

use App\product;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    public function index()
    {
        return view('admin');
    }

    protected function groupPageSettingsValidator(array $data)
    {
        return Validator::make($data, [
            'username' => 'required|unique :admin',
            'password' => 'required',

        ]);
    }


    public function insert(Request $request)
    {
        $request->validate([
            'username' => 'required',
            'password' => 'required',

        ]);


        if (DB::table('admin')->where('username', $request->username)->where('password', $request->password)->first()) {

            return view('admin_details');
        }

        else {

            return redirect()->back()->with ('message',' The password that you\'ve entered is incorrect.');
//            return view('hi');
        }


    }

//    public function check1(Request $request)
//    {
//        $users = DB::table('reg')->where('username', $request->username)->where('password', $request->password)->get();
//        return view('guna/first')->with('users', $users);
//
//    }
}