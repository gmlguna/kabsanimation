<?php

namespace App\Http\Controllers;

use App\product;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class UserloginController extends Controller
{
    public function index()
    {
        return view('userlogin');
    }

    protected function groupPageSettingsValidator(array $data)
    {
        return Validator::make($data, [
            'regno' => 'required|unique :student',
            'dob' => 'required',

        ]);
    }


    public function insert(Request $request)
    {
        $request->validate([
            'regno' => 'required',
            'dob' => 'required',

        ]);

        if ($login = DB::table('student')->where('regno', $request->regno)->where('dob', $request->dob)->first()) {

            $regno = $request->regno;
            return view('ex', compact('regno','login'));

        } else {

            return redirect()->back()->with('message', ' The password that you\'ve entered is incorrect.');
//            return view('hi');
        }




    }

//    public function check1(Request $request)
//    {
//        $users = DB::table('reg')->where('username', $request->username)->where('password', $request->password)->get();
//        return view('guna/first')->with('users', $users);
//
//    }

}

