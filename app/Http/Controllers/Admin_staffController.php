<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class Admin_staffController extends Controller {
    public function index(){
        $users = DB::select('select * from staff');
        return view('admin_registerdetails',['users'=>$users]);
    }
}