<?php

namespace App\Http\Controllers;

use App\product;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class StaffloginController extends Controller
{
    public function index()
    {
        return view('staff_login');
    }

    protected function groupPageSettingsValidator(array $data)
    {
        return Validator::make($data, [
            'username' => 'required|unique :staff',
            'password' => 'required',

        ]);
    }


    public function insert(Request $request)
    {
        $request->validate([
            'username' => 'required',
            'password' => 'required',

        ]);


        if (DB::table('staff')->where('username', $request->username)->where('password', $request->password)->first()) {

            return view('staff_login');

        }
        else {

            return redirect()->back()->with ('msg',' The password that you\'ve entered is incorrect.');
//            return view('traningclass');
        }


    }

//    public function check1(Request $request)
//    {
//        $users = DB::table('reg')->where('username', $request->username)->where('password', $request->password)->get();
//        return view('guna/first')->with('users', $users);
//
//    }
}