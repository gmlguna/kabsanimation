<?php

namespace App\Http\Controllers;

//use Barryvdh\DomPDF\Facade as PDF;
use Illuminate\Http\Request;
use App\product;
use DB;
use App\Http\Requests;

class PdfController extends Controller
{
    public function index($regno)
    {
        $info = DB::table('student')->where('regno', $regno)->first();
        $data = ['regno' =>$info->regno ,
            'name'=>$info->name,
            'startdate'=>$info->startdate,
            'enddate'=>$info->enddate,
            'duration'=>$info->duration,
            'joindate'=>$info->joindate,
            'courses'=>$info->courses];
            $pdf = \PDF::loadView('pdf',compact('data'));
            return $pdf->download();

    }
}
