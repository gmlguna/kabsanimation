<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class Enquire_deleteController extends Controller {
    public function index(){
        $users = DB::select('select * from home');
        return view('enquire/enquire_delete',['users'=>$users]);
    }
    public function destroy($id) {
        DB::delete('delete from home where  id= ?',[$id]);
        return redirect()->back()->with ('message',' Enquire Details Deleted ');
    }
}