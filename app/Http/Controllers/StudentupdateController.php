<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class StudentupdateController extends Controller {
    public function index(){
        $users = DB::select('select * from student');
        return view('student_edit',['users'=>$users]);
    }
    public function show($regno) {
        $users = DB::select('select * from student where regno = ?',[$regno]);
        return view('student_update',['users'=>$users]);
    }
    public function edit(Request $request,$regno) {
        $name = $request->name;
        $dob = $request->dob;
        $email = $request->email;
        $gender = $request->gender;
        $startdate = $request->startdate;
        $enddate = $request->enddate;
        $duration = $request->duration;
        $joindate = $request->joindate;
        $courses = $request->courses;
        $address = $request->address;

        DB::update('update student set name = ?, dob = ?, email = ?, gender = ?, startdate = ?, enddate = ?, duration = ?, joindate = ?, courses = ?, address = ? where regno = ?',[$name, $dob, $email, $gender, $startdate, $enddate, $duration, $joindate, $courses, $address, $regno]);
        return redirect()->back()->with ('message',' Student details upadeted ');
    }
}