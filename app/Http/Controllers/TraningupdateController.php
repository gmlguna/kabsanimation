<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class TraningupdateController extends Controller {
    public function index(){
        $users = DB::select('select * from traning');
        return view('traning/traning_edit',['users'=>$users]);
    }
    public function show($traning) {
        $users = DB::select('select * from traning where traning = ?',[$traning]);
        return view('traning/traning_update',['users'=>$users]);
    }
    public function edit(Request $request,$traning) {
        $name = $request->name;
        $courses = $request->courses;
        $date = $request->date;
        $time = $request->time;

        DB::table('traning')
            ->where('traning', $traning)
            ->update(['name' => $name,'courses' => $courses,'date' => $date,'time' => $time]);

//        DB::update('update staff set name = ?, mobileno = ?, where staff_id = ?',[$name, $mobileno, $staff_id]);
        return redirect()->back()->with ('message',' Traning details upadeted ');
    }
}