<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class StaffupdateController extends Controller {
    public function index(){
        $users = DB::select('select * from staff');
        return view('views/staff',['users'=>$users]);
    }
    public function show($staff_id) {
        $users = DB::select('select * from staff where staff_id = ?',[$staff_id]);
        return view('views/staff_update',['users'=>$users]);
    }
    public function edit(Request $request,$staff_id) {
        $name = $request->name;
        $mobileno = $request->mobileno;
        $dob = $request->dob;
        $email = $request->email;
        $gender = $request->gender;
        $joindate = $request->joindate;
        $address = $request->address;

        DB::table('staff')
            ->where('staff_id', $staff_id)
            ->update(['name' => $name,'mobileno' => $mobileno,'dob' => $dob,'email' => $email,'gender' => $gender,'joindate' => $joindate,'address' => $address]);

//        DB::update('update staff set name = ?, mobileno = ?, where staff_id = ?',[$name, $mobileno, $staff_id]);
        return redirect()->back()->with ('message',' Staff details upadeted ');
    }
}