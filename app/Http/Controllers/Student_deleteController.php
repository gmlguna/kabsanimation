<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class Student_deleteController extends Controller {
    public function index(){
        $users = DB::select('select * from student');
        return view('student_delete',['users'=>$users]);
    }
    public function destroy($regno) {
        DB::delete('delete from student where  regno= ?',[$regno]);
        return redirect()->back()->with ('student',' Student Deleted ');
    }
}