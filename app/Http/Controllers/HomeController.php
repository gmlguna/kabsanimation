<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use App\Http\Controllers\Controller;
//use App\Http\Controllers\Validator;
use Validator;

class HomeController extends Controller
{
    public function insertform()
    {
        return view('home');
    }

    /**
     * @param Request $request
     */

    protected function groupPageSettingsValidator(array $data)
    {
        return Validator::make($data, [
            'location' => 'required|unique :student',
            'company' => 'required',
            'name' => 'required|after:yesterday',
            'email' => 'required|unique:student|email',
            'phone' => 'required',

        ]);
    }


    public function insert(Request $request)
    {

        $request->validate([
            'location' => 'required',
            'company' => 'required',
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',

        ]);


        $location = $request->location;
        $company = $request->company;
        $name = $request->name;
        $email = $request->email;
        $phone = $request->phone;
        $yourmsg = $request->yourmsg;
        $date = $request->date;


//        var_dump($request->all());die;
        DB::insert('insert into home (location, company, name, email, phone, yourmsg, date) values(?,?,?,?,?,?,?)', [$location, $company, $name, $email, $phone, $yourmsg, $date]);

//        echo "Record inserted successfully.";
        return redirect()->back()->with ('message',' Enquire success ');
    }
}